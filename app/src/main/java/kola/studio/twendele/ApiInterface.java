package kola.studio.twendele;

import java.util.ArrayList;
import java.util.Map;

import kola.studio.twendele.Models.FriendRequestStatus;
import kola.studio.twendele.Models.AddHeader;
import kola.studio.twendele.Models.CreateGoingIntent;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.Models.FriendList;
import kola.studio.twendele.Models.FriendRequest;
import kola.studio.twendele.Models.Goings;
import kola.studio.twendele.Models.Header;
import kola.studio.twendele.Models.PendingFriendRequests;
import kola.studio.twendele.Models.Place;
import kola.studio.twendele.Models.User;
import kola.studio.twendele.Models.UserDetails;
import kola.studio.twendele.Models.Users_;
import kola.studio.twendele.Models.Venue;
import kola.studio.twendele.Models.VerifyPhoneNo;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("verifyOtp")
    Call<VerifyPhoneNo> verifyPhoneNo(@Field("otp_code") String otpCode, @Field("mobile_no") String mobile_no);

    @Multipart
    @POST("registration")
    Call<User> registerUser(@PartMap Map<String, String> data, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("login")
    Call<User> login(@Field("mobile_no") String mobileNo,@Field("password") String password);

    @GET("getVenue")
    Call<Venue> getNearbyPlaces(@Query("latitude") double lat,@Query("longitude") double lng);

    @GET("me/")
    Call<UserDetails> getUserProfile();

    @GET("feed")
    Call<Goings> feed();

    @GET("places/")
    Call<ArrayList<Place>> getNearbyPlaces(@Query("latitude") String latitude, @Query("longitude") String longitude);

    @GET("places/search")
    Call<ArrayList<Data>> searchVenue(@Query("venueName") String venueName, @Query("latitude") String latitude, @Query("longitude")String longitude);

    @GET("me/goings/")
    Call<Goings> myFeed();

    @FormUrlEncoded
    @POST("me/goings/create")
    Call<CreateGoingIntent> createGoingIntent(@Field("placeId") String placeId, @Field("date") String date, @Field("time") String time);

    @Multipart
    @POST("header")
    Call<AddHeader> addHeaderContent(@PartMap Map<String, String> data, @Part MultipartBody.Part file);

    @GET("header")
    Call<Header> getHeaderContent();

    @GET("me/friends")
    Call<ArrayList<FriendList>> getListOfFriends();

    @GET("me/getFriendRequests")
    Call<ArrayList<PendingFriendRequests>> getPendingFriendRequests();

    @FormUrlEncoded
    @POST("me/sendfriendRequests")
    Call<FriendRequest> sendFriendRequest(@Field("recipient_id") long id);

    @FormUrlEncoded
    @POST("me/acceptFriend")
    Call<FriendRequestStatus> acceptFriendRequest(@Field("sender_id") long id);

    @FormUrlEncoded
    @POST("me/rejectFriendRequest")
    Call<FriendRequestStatus> rejectFriendRequest(@Field("sender_id") long id);

    @GET("me/people")
    Call<ArrayList<Users_>> getUsers();
}
