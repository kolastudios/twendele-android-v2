package kola.studio.twendele.ui.User;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.HashMap;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.AddHeader;
import kola.studio.twendele.Models.Header;
import kola.studio.twendele.Models.UserDetails;
import kola.studio.twendele.Utilities.Utils;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserViewModel extends ViewModel {
    private MutableLiveData<UserDetails> userDetailsMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Header> headerMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<AddHeader> addHeaderMutableLiveData = new MutableLiveData<>();

    private ApiInterface apiInterface;

    public LiveData<UserDetails> getUserDetails() { return userDetailsMutableLiveData; }

    LiveData<Header> getHeader() { return headerMutableLiveData; }

    LiveData<AddHeader> getHeader1() { return addHeaderMutableLiveData; }

    public void getUserProfile(Context context){
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<UserDetails> call = apiInterface.getUserProfile();
        call.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Call<UserDetails> call, Response<UserDetails> response) {
                Utils.log("responseMessage->"+response.message());
                Utils.log("responseBody->"+response.message());
                Utils.log("responseCode->"+response.code());
                UserDetails  userDetails = response.body();
                if(userDetails != null){
                    userDetailsMutableLiveData.setValue(userDetails);
                }else{
                    userDetailsMutableLiveData.setValue(null);
                }
            }
            @Override
            public void onFailure(Call<UserDetails> call, Throwable t) {
                userDetailsMutableLiveData.setValue(null);
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }

    public void getHeaderContent(Context context){
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<Header> call = apiInterface.getHeaderContent();
        call.enqueue(new Callback<Header>() {
            @Override
            public void onResponse(Call<Header> call, Response<Header> response) {
                Utils.log("responseMessage->"+response.message());
                Utils.log("responseBody->"+response.message());
                Utils.log("responseCode->"+response.code());
                Header header = response.body();
                if(header != null) {
                    headerMutableLiveData.setValue(header);
                }else{
                    headerMutableLiveData.setValue(null);
                }
            }
            @Override
            public void onFailure(Call<Header> call, Throwable t) {
                headerMutableLiveData.setValue(null);
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }

    public void addHeaderContent(Context context,String bio,MultipartBody.Part filePart) {
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        HashMap<String, String> data = new HashMap<>();
        data.put("about_me", bio);
        Call<AddHeader> call = apiInterface.addHeaderContent(data, filePart);
        call.enqueue(new Callback<AddHeader>() {
            @Override
            public void onResponse(Call<AddHeader> call, Response<AddHeader> response) {
                AddHeader addHeader = response.body();
                if(addHeader != null){
                    addHeaderMutableLiveData.setValue(addHeader);
                }else{
                    addHeaderMutableLiveData.setValue(null);
                }
            }
            @Override
            public void onFailure(Call<AddHeader> call, Throwable t) {
                addHeaderMutableLiveData.setValue(null);
                Utils.log("Error connecting to server" + t.getMessage());
            }
        });
    }
}
