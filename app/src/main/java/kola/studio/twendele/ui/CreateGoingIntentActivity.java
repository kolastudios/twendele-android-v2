package kola.studio.twendele.ui;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.aquery.AQuery;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.APIClient;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.CreateGoingIntent;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.R;
import kola.studio.twendele.TaskLoadedCallback;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateGoingIntentActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMyLocationChangeListener, TaskLoadedCallback {


    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    EditText date;
    EditText time;
    Button submitBtn;
    AQuery aQuery;
    GoogleMap mMap;
    Data data;
    MarkerOptions markerOptions;
    MarkerOptions markerOptions1;
    Polyline currentPolyLine;
    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_going_intent);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);

        SlidingUpPanelLayout layout = findViewById(R.id.slidingPanel);
        layout.setAnchorPoint(0.1f);
        layout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.place_details_map);
        mapFragment.getMapAsync(this);
        data = (Data) getIntent().getSerializableExtra("DATA");
        aQuery = new AQuery(this);
        bindViews();
        setViewActions();
        showDatePickerDialog();
        showTimePickerDialog();
        setPlaceDetails(data);
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setOnMarkerClickListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMyLocationChangeListener(this);
        addMarkerToPlace(data);
    }

    private void setPlaceDetails(Data data){
        aQuery.id(R.id.place_name).text(data.getName());
        aQuery.id(R.id.place_address).text(data.getAddress());
        aQuery.id(R.id.place_country).text(data.getCountry());
        if(data.getCategories().size() > 0){
            aQuery.id(R.id.categoryName).text(data.getCategories().get(0).getName());
            aQuery.id(R.id.categoryShortName).text(data.getCategories().get(0).getShortName());
        }
        if(data.getFriendsGoing() != null && data.getPeopleGoing() != null){
            aQuery.id(R.id.people_going).text(data.getPeopleGoing().toString());
            aQuery.id(R.id.friends_going).text(String.valueOf(data.getFriendsGoing().size()));
            Intent intent = new Intent(this, FriendsGoingActivity.class);
            aQuery.id(R.id.friends_going).click(view -> {
                intent.putExtra("FRIENDS", data);
                startActivity(intent);
            });
        }
    }

    private String convertTo24HrFormat(int val){
        if(val<10){
            return "0"+val;
        }
        return ""+val;
    }
    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.set_going));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(view -> {
                aQuery.openFromLeft(GoingActivity.class);
                finish();
            });
        }
    }

    private void bindViews(){
        date = findViewById(R.id.date_);
        time = findViewById(R.id.time_);
        submitBtn = findViewById(R.id.go_btn);
    }

    private void setViewActions(){
        date.setOnClickListener(this);
        time.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.date_:
                datePickerDialog.show();
                break;
            case R.id.time_:
                timePickerDialog.show();
                break;
            case R.id.go_btn:
                createGoingIntent(data,date.getText().toString(),time.getText().toString());
                break;
        }
    }

    private void showDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(CreateGoingIntentActivity.this,android.R.style.Theme_Holo_Light_Dialog, (view, year, month, dayOfMonth) -> {
            date.setText(year+"-"+(month+1)+"-"+dayOfMonth);
            datePickerDialog.dismiss();
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        Utils.log("min date"+(calendar.get(Calendar.YEAR)-1));
    }

    private void showTimePickerDialog(){
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int seconds = c.get(Calendar.SECOND);
        // Create a new instance of TimePickerDialog and return it
        timePickerDialog = new TimePickerDialog(CreateGoingIntentActivity.this, (timePicker, sHour, sMinute) -> {
            String timeFomat = convertTo24HrFormat(sHour)+":"+convertTo24HrFormat(sMinute)+":"+convertTo24HrFormat(seconds);
            SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());
            try{
                time.setText(new SimpleDateFormat("HH:mm:ss",Locale.getDefault()).format(simpleTimeFormat.parse(timeFomat)));
            }catch (Exception e){
                e.printStackTrace();
            }
            timePickerDialog.dismiss();
        },hour,minute,true);
    }

    private void addMarkerToPlace(Data data){
        double lat = data.getLatitude();
        double lng = data.getLongitude();
        String venue = data.getName();
        LatLng location = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(location).title(venue)).setTag(0);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(location);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
        mMap.moveCamera(cameraUpdate);
        mMap.animateCamera(zoom);
    }

    @Override
    public void onMyLocationChange(Location location) {
        markerOptions = new MarkerOptions().position(new LatLng(location.getLatitude(),location.getLongitude())).title("Me");
        markerOptions1 = new MarkerOptions().position(new LatLng(data.getLatitude(),data.getLongitude())).title(data.getName());
        String url = getDirectionsUrl(markerOptions.getPosition(),markerOptions1.getPosition(),"driving");
//        new FetchUrl(CreateGoingIntentActivity.this).execute(url,"driving");
    }

    public String getDirectionsUrl(LatLng orig,LatLng dest,String dMode){
        String origin = "origin="+orig.latitude+","+orig.longitude;
        String destination ="destination="+dest.latitude+","+dest.longitude;
        String outputFormat = "json";
        String mode = "mode="+dMode;
        String parameters=origin + "&" + destination + "&" + mode + "&key=" + getString(R.string.google_maps_key);
        return "https://maps.googleapis.com/maps/api/directions/" + outputFormat + "?" + parameters;
    }

    @Override
    public void onTaskDone(Object... values) {
        if(currentPolyLine != null){
            currentPolyLine.remove();
            currentPolyLine = mMap.addPolyline((PolylineOptions) values[0]);
        }
    }

    private void createGoingIntent(Data data,String date,String time){
        apiInterface = APIClient.getClient(CreateGoingIntentActivity.this).create(ApiInterface.class);
        Call<CreateGoingIntent> call = apiInterface.createGoingIntent(data.getPlaceId(),date,time);
        call.enqueue(new Callback<CreateGoingIntent>() {
            @Override
            public void onResponse(Call<CreateGoingIntent> call, Response<CreateGoingIntent> response) {
                Utils.log("response_msg : "+response.body());
                Utils.log("response code : "+response.code());
                if(response.isSuccessful()){
                    Utils.log("response_msg : "+response.body());
                    Utils.log("response code : "+response.code());
                    Intent intent =  new Intent(CreateGoingIntentActivity.this, GoingActivity.class);
                    intent.putExtra("STATUS",getResources().getString(R.string.notification));
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }else{
                    Toasty.error(CreateGoingIntentActivity.this, "Failed.", Toast.LENGTH_SHORT, true).show();
                }
            }
            @Override
            public void onFailure(Call<CreateGoingIntent> call, Throwable t) {
                Utils.log("Error connecting to server" + t.getMessage());
                Toasty.error(CreateGoingIntentActivity.this, "Error connecting to server", Toasty.LENGTH_SHORT, true).show();
            }
        });
    }
}
