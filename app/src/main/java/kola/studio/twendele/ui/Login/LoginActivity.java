package kola.studio.twendele.ui.Login;

import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.kaopiz.kprogresshud.KProgressHUD;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.Models.User;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.Utilities.Helper;
import kola.studio.twendele.Utilities.NetworkLocationProvider;
import kola.studio.twendele.ui.GoingActivity;
import kola.studio.twendele.ui.Register.RegisterActivity;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;

public class LoginActivity extends AppCompatActivity {

    private Button loginButton;
    AQuery aQuery;
    CountryCodePicker ccpCountry;
    EditText mobileNumber,password;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    LoginViewModel loginViewModel;
    private TextView registerUser;
    private KProgressHUD kProgressHUD;
    private View rootView;
    private NetworkLocationProvider networkLocationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        aQuery = new AQuery(this);
        bindViews();
        rootView = findViewById(R.id.loginView);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        if(Helper.isUserLoggedIn()){
            logInCurrentUser();
        }

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(LoginFormState loginFormState) {
                if(loginFormState == null){
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if(loginFormState.getmobileNumberError() != null){
                    mobileNumber.setError(getString(loginFormState.getmobileNumberError()));
                }
                if(loginFormState.getPasswordError() != null){
                    password.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                kProgressHUD.dismiss();
                if(user == null){
                    Snackbar.make(rootView,"Invalid credentials",Snackbar.LENGTH_SHORT).show();
                }else{
                    logInCurrentUser();
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(
                        ccpCountry.getFullNumber(),
                        password.getText().toString()
                );
            }
        };
        mobileNumber.addTextChangedListener(afterTextChangedListener);
        password.addTextChangedListener(afterTextChangedListener);

        aQuery.view(loginButton).click(view -> { loginUser(); });
        aQuery.view(registerUser).click(view -> { aQuery.openFromRight(RegisterActivity.class);});
    }

    private void logInCurrentUser(){
        aQuery.openFromRight(GoingActivity.class);
        finish();
    }
    private void bindViews(){
        mobileNumber = findViewById(R.id.mobile_no);
        ccpCountry = findViewById(R.id.ccp);
        ccpCountry.registerCarrierNumberEditText(mobileNumber);
        password = findViewById(R.id.password);
        loginButton = findViewById(R.id.login_btn);
        registerUser = findViewById(R.id.txt_signup);
    }

    private void loginUser(){
        if(!Twendele.isDeviceOnline()){
            Helper.showAlert(this,"Alert","No internet connection");
            return;
        }
        kProgressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Logging in")
                .setCancellable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.8f)
                .show();
        loginViewModel.login(this,ccpCountry.getFullNumber(),password.getText().toString());
    }
}
