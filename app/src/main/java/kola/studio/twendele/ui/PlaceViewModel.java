package kola.studio.twendele.ui;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.Models.Place;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceViewModel extends ViewModel {

    private MutableLiveData<ArrayList<Data>> data = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Place>> nearByPlaces = new MutableLiveData<>();
    private ApiInterface apiInterface;

    LiveData<ArrayList<Data>> getData(){ return data;}
    LiveData<ArrayList<Place>> nearByPlaces(){ return nearByPlaces;}

    public void searchVenue(Context context,String query,String latitude,String longitude){
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<ArrayList<Data>> call = apiInterface.searchVenue(query,latitude,longitude);
        call.enqueue(new Callback<ArrayList<Data>>() {
            @Override
            public void onResponse(Call<ArrayList<Data>> call, Response<ArrayList<Data>> response) {
                if(response.isSuccessful()){
                    ArrayList<Data> venue = response.body();
                    if(venue != null){
                        data.setValue(venue);
                    }else{
                        data.setValue(null);
                    }
                }else{
                    data.setValue(null);
                }
            }
            @Override
            public void onFailure(Call<ArrayList<Data>> call, Throwable t) {
                data.setValue(null);
                Utils.log("Error connecting to web server" + t.getMessage());
            }
        });
    }

    public void getNearByPlaces(Context context,String latitude, String longitude) {
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<ArrayList<Place>> call = apiInterface.getNearbyPlaces(latitude, longitude);
        call.enqueue(new Callback<ArrayList<Place>>() {
            @Override
            public void onResponse(Call<ArrayList<Place>> call, Response<ArrayList<Place>> response) {
                if(response.isSuccessful()){
                    ArrayList<Place> places = response.body();
                    if(places != null){
                        nearByPlaces.setValue(places);
                    }else {
                        nearByPlaces.setValue(null);
                    }
                }else{
                    nearByPlaces.setValue(null);
                }
            }
            @Override
            public void onFailure(Call<ArrayList<Place>> call, Throwable t) {
                nearByPlaces.setValue(null);
                Utils.log("Error connecting to server " + t.getMessage());
            }
        });
    }


}
