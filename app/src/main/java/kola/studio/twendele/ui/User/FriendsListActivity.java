package kola.studio.twendele.ui.User;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import kola.studio.twendele.Adapters.FriendListAdapter;
import kola.studio.twendele.Models.FriendList;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;

public class FriendsListActivity extends AppCompatActivity {

    private FriendListAdapter friendListAdapter;
    private ListView listView;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private View view;
    private AQuery aQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);
        toolbar = findViewById(R.id.toolbar);
        aQuery = new AQuery(this);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);
        ArrayList<FriendList> friendList = (ArrayList<FriendList>) getIntent().getSerializableExtra("FRIENDS");
        view = findViewById(R.id.friendListView);
        Utils.log("userProfile->"+friendList);
        progressBar = findViewById(R.id.progress_circular);
        friendsList(friendList);
    }

    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.friends));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorTransparent));
            toolbar.setNavigationOnClickListener(view -> {
                aQuery.openFromLeft(MyProfile.class);
                finish();
            });
        }
    }

    private void friendsList(ArrayList<FriendList> friendList) {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        if(friendList.size() < 1){
            progressBar.setVisibility(View.VISIBLE);
            Snackbar.make(view,"You have no friends",Snackbar.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
        }
        progressBar.setVisibility(View.INVISIBLE);
        friendListAdapter = new FriendListAdapter(this, R.layout.friends_list, friendList);
        listView = findViewById(R.id.listView);
        listView.setAdapter(friendListAdapter);
    }
}
