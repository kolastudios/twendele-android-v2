package kola.studio.twendele.ui.User;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import kola.studio.twendele.Adapters.GoingsAdapter;
import kola.studio.twendele.Adapters.TabAdapter;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.ui.CustomPopUpWindow;
import kola.studio.twendele.ui.GoingActivity;
import kola.studio.twendele.ui.ProfilePicture;

public class MyProfile extends AppCompatActivity {
    ApiInterface apiInterface;
    private GoingsAdapter goingsAdapter;
    private ListView listView;
    AQuery aQuery;
    TabLayout tabLayout;
    private ViewPager viewPager;
    private TabAdapter tabAdapter;
    private ProgressBar progressBar;
    private View view;
    private UserViewModel userViewModel;
    private FriendsViewModel friendsViewModel;
    private View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);
        aQuery = new AQuery(this);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.view_pager);
        rootView = findViewById(R.id.my_profile);
        tabAdapter = new TabAdapter(getSupportFragmentManager());
        tabAdapter.addFragment(new FragmentMyGoings(),getResources().getString(R.string.myGoings));
        viewPager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(viewPager);

        friendsViewModel = ViewModelProviders.of(this).get(FriendsViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        friendsViewModel.getFriends().observe(this, friendLists -> {
            if(friendLists == null){
                Snackbar.make(view,"Error occured while loading data",Snackbar.LENGTH_SHORT).show();
                finish();
            }else{
                aQuery.id(R.id.friends_list).text(friendLists.get(0).getNumberOfFriends().toString()).click(view -> {
                    Intent intent =  new Intent(MyProfile.this,FriendsListActivity.class);
                    intent.putExtra("FRIENDS",friendLists);
                    aQuery.openFromRight(intent);
                });
            }
        });

        userViewModel.getUserDetails().observe(this, userDetails -> {
            if(userDetails == null){
                Snackbar.make(view,"Error occured while loading data",Snackbar.LENGTH_SHORT).show();
                finish();
            }else {
                aQuery.id(R.id.uname).text(userDetails.getUserProfile().getFullName());
                aQuery.id(R.id.txt_location).text(userDetails.getUserProfile().getAddress());
                aQuery.id(R.id.profile_image1).image(userDetails.getUrl()).click(view -> new CustomPopUpWindow(view,MyProfile.this).popUpWindow(userDetails));
            }
        });
        userViewModel.getHeader().observe(this, header -> {
            if(header == null){
                Snackbar.make(view,"Error occured while loading data",Snackbar.LENGTH_SHORT).show();
                finish();
            }else {
                aQuery.id(R.id.bio).text(header.getHeaderContent().getAboutUser());
                aQuery.id(R.id.header_background).image(header.getUrl()).click(view -> {
                    Intent intent = new Intent(MyProfile.this, ProfilePicture.class);
                    intent.putExtra("profile_pic", header.getUrl());
                    startActivity(intent);
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        fetchUserDetails();
        fetchFriends();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchUserDetails();
    }

    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorTransparent));
            toolbar.setNavigationOnClickListener(view -> {
                aQuery.openFromLeft(GoingActivity.class);
                finish();
            });
        }
    }

    private void fetchUserDetails(){
        if(!Twendele.isDeviceOnline()){
            Snackbar.make(rootView,"No Internet connection",Snackbar.LENGTH_SHORT).show();
            return;
        }
        userViewModel.getHeaderContent(this);
        userViewModel.getUserProfile(this);
    }

    private void fetchFriends(){
        if(!Twendele.isDeviceOnline()){
            return;
        }
        friendsViewModel.getUsersFriends(this);
    }
}
