package kola.studio.twendele.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.squareup.picasso.Picasso;

import kola.studio.twendele.Models.FriendList;
import kola.studio.twendele.Models.PendingFriendRequests;
import kola.studio.twendele.Models.UserDetails;
import kola.studio.twendele.Models.Users_;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;

public class CustomPopUpWindow extends Activity {

    private View view;
    private Context context;

    public CustomPopUpWindow(View view, Context context) {
        this.view = view;
        this.context = context;
    }

    public void popUpWindow(UserDetails userDetails){
        //Create a View object yourself through inflater
        LayoutInflater inflater = (LayoutInflater) this.view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler
        ImageView imageView = popupView.findViewById(R.id.profile_image_container);
        Utils.log("image url->"+ userDetails);
        Utils.log("username->"+ userDetails);
        Picasso.get().load(userDetails.getUrl()).into(imageView);

        //Handler for clicking on the inactive zone of the window
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchInterceptor((view1, motionEvent) -> {
            if(motionEvent.getAction() == MotionEvent.ACTION_OUTSIDE){
                popupWindow.dismiss();
            }
            return false;
        });

        //open profile activity
        popupView.setOnTouchListener((v, event) -> {
            Intent intent = new Intent(context, ProfilePicture.class);
            intent.putExtra("profile_pic", userDetails.getUrl());
            intent.putExtra("full_name", userDetails.getUserProfile().getFullName());
            context.startActivity(intent);
            popupWindow.dismiss();
            return true;
        });
        dimBehind(popupWindow);
    }

    public static void dimBehind(PopupWindow popupWindow) {
        try {
            View container;
            if (popupWindow.getBackground() == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = (View) popupWindow.getContentView().getParent();
                } else {
                    container = popupWindow.getContentView();
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container = (View) popupWindow.getContentView().getParent().getParent();
                } else {
                    container = (View) popupWindow.getContentView().getParent();
                }
            }
            Context context = popupWindow.getContentView().getContext();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            p.dimAmount = 0.7f;
            wm.updateViewLayout(container, p);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void popUpWindowForListView(FriendList friend){
        //Create a View object yourself through inflater
        LayoutInflater inflater = (LayoutInflater) this.view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler
        ImageView imageView = popupView.findViewById(R.id.profile_image_container);
//        Utils.log("image url->"+friend.getFbProfileUrl());
        Utils.log("username->"+friend.getFriend().getFullName());
        Picasso.get().load(friend.getUrl()).into(imageView);

        //Handler for clicking on the inactive zone of the window
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchInterceptor((view1, motionEvent) -> {
            if(motionEvent.getAction() == MotionEvent.ACTION_OUTSIDE){
                popupWindow.dismiss();
            }
            return false;
        });

        //open profile activity
        popupView.setOnTouchListener((v, event) -> {
            Intent intent = new Intent(context, ProfilePicture.class);
            intent.putExtra("profile_pic",friend.getUrl());
            intent.putExtra("full_name",friend.getFriend().getFullName());
            context.startActivity(intent);
            popupWindow.dismiss();
            return true;
        });
        dimBehind(popupWindow);
    }
    public void popUpWindowForFriendRequest(PendingFriendRequests friend){
        //Create a View object yourself through inflater
        LayoutInflater inflater = (LayoutInflater) this.view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler
        ImageView imageView = popupView.findViewById(R.id.profile_image_container);
//        Utils.log("image url->"+friend.getFbProfileUrl());
        Utils.log("username->"+friend.getSender().getFullName());
        Picasso.get().load(friend.getUrl()).into(imageView);

        //Handler for clicking on the inactive zone of the window
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchInterceptor((view1, motionEvent) -> {
            if(motionEvent.getAction() == MotionEvent.ACTION_OUTSIDE){
                popupWindow.dismiss();
            }
            return false;
        });

        //open profile activity
        popupView.setOnTouchListener((v, event) -> {
            Intent intent = new Intent(context, ProfilePicture.class);
            intent.putExtra("profile_pic",friend.getUrl());
            intent.putExtra("full_name",friend.getSender().getFullName());
            context.startActivity(intent);
            popupWindow.dismiss();
            return true;
        });
        dimBehind(popupWindow);
    }
    public void popUpWindowForSendRequest(Users_ users_){
        //Create a View object yourself through inflater
        LayoutInflater inflater = (LayoutInflater) this.view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler
        ImageView imageView = popupView.findViewById(R.id.profile_image_container);
//        Utils.log("image url->"+friend.getFbProfileUrl());
        Utils.log("username->"+users_.getPeople().getFullName());
        Picasso.get().load(users_.getUrl()).into(imageView);

        //Handler for clicking on the inactive zone of the window
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchInterceptor((view1, motionEvent) -> {
            if(motionEvent.getAction() == MotionEvent.ACTION_OUTSIDE){
                popupWindow.dismiss();
            }
            return false;
        });

        //open profile activity
        popupView.setOnTouchListener((v, event) -> {
            Intent intent = new Intent(context, ProfilePicture.class);
            intent.putExtra("profile_pic",users_.getUrl());
            intent.putExtra("full_name",users_.getPeople().getFullName());
            context.startActivity(intent);
            popupWindow.dismiss();
            return true;
        });
        dimBehind(popupWindow);
    }
}
