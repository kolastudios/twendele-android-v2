package kola.studio.twendele.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import kola.studio.twendele.Adapters.FriendsGoingAdapter;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.R;

public class FriendsGoingActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FriendsGoingAdapter friendsGoingAdapter;
    private ListView listView;
    private Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_going);
        toolbar = findViewById(R.id.toolbar);
        toolbarSetUp(toolbar);
        data = (Data) getIntent().getSerializableExtra("FRIENDS");
        populateFriendsgoing(data);
    }
    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.friends_going));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorTransparent));
            toolbar.setNavigationOnClickListener(v -> finish());
        }
    }

    private void populateFriendsgoing(Data data){
        friendsGoingAdapter = new FriendsGoingAdapter(this, R.layout.friends_list,data);
        listView = findViewById(R.id.listView);
        listView.setAdapter(friendsGoingAdapter);
    }
}
