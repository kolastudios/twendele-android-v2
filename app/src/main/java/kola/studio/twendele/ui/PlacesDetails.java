package kola.studio.twendele.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.github.ybq.android.spinkit.style.FadingCircle;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.APIClient;
import kola.studio.twendele.Adapters.GoingAdapter;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.Goings;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlacesDetails extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private RecyclerView recyclerView;
    private GoingActivity goingActivity;
    private GoingAdapter goingAdapter;
    private ProgressBar progressBar;
    private ApiInterface apiInterface;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places_details);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment  mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this::onMapReady);
//        feed();

    }



    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            Intent intent = getIntent();
            String[] venue_data = intent.getStringArrayExtra("venue_data");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(venue_data[0]);
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(v -> finish());
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        Intent intent = getIntent();
        String[] venue_data = intent.getStringArrayExtra("venue_data");
        Utils.log("venue_data->"+venue_data);
        Utils.log("title->"+venue_data[0]);
        Utils.log("lat->"+venue_data[1]);
        Utils.log("long->"+venue_data[2]);

        double lat = Double.parseDouble(venue_data[1]);
        double lng = Double.parseDouble(venue_data[2]);
        String venue = venue_data[0];
        LatLng location = new LatLng(lat,lng);
        mMap.addMarker(new MarkerOptions().position(location).title(venue));

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(location);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(18);
        mMap.moveCamera(cameraUpdate);
        mMap.animateCamera(zoom);
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void feed(){
        progressBar = findViewById(R.id.spin_kit);
        FadingCircle fadingCircle = new FadingCircle();
        progressBar.setIndeterminateDrawable(fadingCircle);
        progressBar.setIndeterminate(true);
        apiInterface = APIClient.getClient(this).create(ApiInterface.class);
        Call<Goings> call = apiInterface.feed();
        call.enqueue(new Callback<Goings>() {
            @Override
            public void onResponse(Call<Goings> call, Response<Goings> response) {
                Utils.log("goings response "+response.body());
                try{
                    Utils.log("goings response for places details "+response.body());
                    progressBar.setVisibility(View.INVISIBLE);
                    Utils.log("response body -> "+response.body());
//                    showGoings(response.body());
                }catch (Exception e){
                    Toasty.error(PlacesDetails.this,"Cannot load goings at this time.",Toasty.LENGTH_SHORT,true).show();
                }
            }
            @Override
            public void onFailure(Call<Goings> call, Throwable t) {
                progressBar.setVisibility(View.VISIBLE);
                Utils.log("Error connecting to server"+t.getMessage());
                Toasty.error(PlacesDetails.this,"Error connecting to server",Toasty.LENGTH_SHORT,true).show();
            }
        });
    }

//    private void showGoings(Goings body) {
//
//        goingAdapter = new GoingAdapter(PlacesDetails.this,body.getData());
//        recyclerView.setAdapter(goingAdapter);
//    }
}
