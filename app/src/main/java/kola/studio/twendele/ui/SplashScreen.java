package kola.studio.twendele.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.aquery.AQuery;
import com.pixplicity.easyprefs.library.Prefs;

import kola.studio.twendele.AccountManager;
import kola.studio.twendele.R;
import kola.studio.twendele.ui.Login.LoginActivity;

public class SplashScreen extends AppCompatActivity {
    Handler handler;
    private final long TIMEOUT=3000;
    AQuery  aQuery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        handler = new Handler();
        aQuery = new AQuery(this);
        handler.postDelayed(() -> {
            if(!Prefs.getString(AccountManager.TOKEN,"").isEmpty()){
                startActivity(new Intent(SplashScreen.this, GoingActivity.class));
            }
            startActivity(new Intent(SplashScreen.this, LoginActivity.class));
            finish();
        },TIMEOUT);
    }
}
