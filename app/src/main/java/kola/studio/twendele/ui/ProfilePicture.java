package kola.studio.twendele.ui;

import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import kola.studio.twendele.R;

public class ProfilePicture extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_picture);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        String fullName = getIntent().getStringExtra("full_name");
        String profilePicture = getIntent().getStringExtra("profile_pic");
        PhotoView photoView = findViewById(R.id.full_size_photo);
        Picasso.get().load(profilePicture).into(photoView);
        toolbarSetUp(toolbar,fullName);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void toolbarSetUp(Toolbar toolbar, String name) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(name);
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.black));
            toolbar.setNavigationOnClickListener(v -> finish());
        }
    }
}
