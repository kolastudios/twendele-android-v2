package kola.studio.twendele.ui.Register;

import androidx.annotation.Nullable;

public class RegisterFormState {

    @Nullable
    private Integer mobileNumberError;
    @Nullable
    private Integer passwordError;
    @Nullable
    private Integer nameError;
    @Nullable
    private Integer genderError;
    @Nullable
    private Integer dateOfBirthError;
    @Nullable
    private Integer imageError;
    @Nullable
    private Integer homeAddressError;

    private boolean isDataValid;

    public RegisterFormState(@Nullable Integer mobileNumberError, @Nullable Integer passwordError,
                             @Nullable Integer nameError, @Nullable Integer genderError,
                             @Nullable Integer dateOfBirthError, @Nullable Integer imageError,
                             @Nullable Integer homeAddressError) {
        this.mobileNumberError = mobileNumberError;
        this.passwordError = passwordError;
        this.nameError = nameError;
        this.genderError = genderError;
        this.dateOfBirthError = dateOfBirthError;
        this.imageError = imageError;
        this.homeAddressError = homeAddressError;
        this.isDataValid =false;
    }

    public RegisterFormState(boolean isDataValid) {
        this.mobileNumberError = null;
        this.passwordError = null;
        this.nameError = null;
        this.genderError = null;
        this.dateOfBirthError = null;
        this.imageError = null;
        this.homeAddressError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    public Integer getMobileNumberError() {
        return mobileNumberError;
    }

    @Nullable
    public Integer getPasswordError() {
        return passwordError;
    }

    @Nullable
    public Integer getNameError() {
        return nameError;
    }

    @Nullable
    public Integer getGenderError() {
        return genderError;
    }

    @Nullable
    public Integer getDateOfBirthError() {
        return dateOfBirthError;
    }

    @Nullable
    public Integer getImageError() {
        return imageError;
    }

    @Nullable
    public Integer getHomeAddressError() {
        return homeAddressError;
    }

    public boolean isDataValid() {
        return isDataValid;
    }
}
