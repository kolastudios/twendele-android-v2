package kola.studio.twendele.ui.VerifyPhoneNumber;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aquery.AQuery;
import com.chaos.view.PinView;
import com.google.android.material.snackbar.Snackbar;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.Objects;

import kola.studio.twendele.AccountManager;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.VerifyPhoneNo;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.Utilities.Helper;
import kola.studio.twendele.ui.Login.LoginActivity;

public class MobileNoVerification extends AppCompatActivity {
    KProgressHUD kProgressHUD;
    PinView pinView;
    ApiInterface apiInterface;
    AQuery aQuery;
    private PhoneVerificationViewModel phoneVerificationViewModel;
    private View rootView;
    Button verifyBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_no_verification);
        aQuery = new AQuery(this);
        rootView = findViewById(R.id.verification_code_view);
        pinView = findViewById(R.id.pinView);
        verifyBtn = findViewById(R.id.verifyBtn);
        phoneVerificationViewModel = ViewModelProviders.of(this).get(PhoneVerificationViewModel.class);

        phoneVerificationViewModel.getPhoneNumber().observe(this, new Observer<VerifyPhoneNo>() {
            @Override
            public void onChanged(VerifyPhoneNo verifyPhoneNo) {
                kProgressHUD.dismiss();
                if(verifyPhoneNo == null){
                    Snackbar.make(rootView,"Invalid verification code",Snackbar.LENGTH_SHORT).show();
                }else{
                    login();
                }
            }
        });
        aQuery.view(verifyBtn).click(view -> verifyPhone());
    }

    private void verifyPhone() {
        if(!Twendele.isDeviceOnline()){
            Helper.showAlert(this,"Alert","No internet connection");
            return;
        }
        kProgressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Verifying mobile number")
                .setCancellable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.8f)
                .show();
        String mobileNumber = Prefs.getString(AccountManager.MOBILE,"");
        phoneVerificationViewModel.verifyMobileNo(this, Objects.requireNonNull(pinView.getText()).toString(),mobileNumber);
    }

    private void login() {
        aQuery.openFromLeft(LoginActivity.class);
        finish();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equalsIgnoreCase("otp")){
                if(!Twendele.isDeviceOnline()){
                    Helper.showAlert(context,"Alert","No internet connection");
                    return;
                }
                kProgressHUD = KProgressHUD.create(MobileNoVerification.this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("Verifying mobile number")
                        .setCancellable(true)
                        .setAnimationSpeed(1)
                        .setDimAmount(0.8f)
                        .show();
                final String message = intent.getStringExtra("message");
                pinView.setText(message);
                String mobileNumber = Prefs.getString(AccountManager.MOBILE,"");

                phoneVerificationViewModel.verifyMobileNo(context, Objects.requireNonNull(pinView.getText()).toString(),mobileNumber);
            }
        }
    };

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(MobileNoVerification.this).
                registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(MobileNoVerification.this).unregisterReceiver(receiver);
    }
}
