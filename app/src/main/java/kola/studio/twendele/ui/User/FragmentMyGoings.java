package kola.studio.twendele.ui.User;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Objects;

import kola.studio.twendele.Adapters.GoingsAdapter;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.Models.Goings;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.Utilities.Utils;


public class FragmentMyGoings extends Fragment{

    private Context context;
    private View view;
    private ProgressBar progressBar;
    private MyGoingViewModel myGoingViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_goings, container, false);
        AQuery aQuery = new AQuery(Objects.requireNonNull(getActivity()));
        progressBar = view.findViewById(R.id.progress_circular);
        aQuery.id(R.id.edit_btn).click(view -> {
            startActivity(new Intent(getActivity(), ActivityHeaderContent.class));
        });
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myGoingViewModel = ViewModelProviders.of(this).get(MyGoingViewModel.class);
        myGoingViewModel.getGoings().observe(this, goings -> {
            progressBar.setVisibility(View.INVISIBLE);
            if(goings == null){
                Snackbar.make(view,"An error occured while loading your goings.",Snackbar.LENGTH_SHORT).show();
            }else{
                listMyGoings(goings);
            }
        });
        fetchMyGoings();
    }

    private void listMyGoings(Goings goings) {
        ArrayList<Data> results = new ArrayList<>();
        if(goings.getData().size() < 1){
            Snackbar.make(view,"No Goings at the moment",Snackbar.LENGTH_SHORT).show();
        }else {
            for (Data data : goings.getData()) {
                data.getCategories();
                data.getAddress();
                data.getFriendsGoing();
                data.getPeopleGoing();
                data.getPlaceId();
                data.getCountry();
                data.getName();
                data.getLatitude();
                data.getLongitude();
                results.add(data);
                Utils.log("My goings");
                Utils.log("place id" + data.getPlaceId());
                Utils.log("country" + data.getCountry());
                Utils.log("End of my goings");
            }
            GoingsAdapter goingsAdapter = new GoingsAdapter(context, R.layout.fragment_my_goings, results);
            ListView listView = view.findViewById(R.id.listView);
            listView.setAdapter(goingsAdapter);
        }
    }

    public void fetchMyGoings(){
        if(!Twendele.isDeviceOnline()){
            return;
        }
        myGoingViewModel.myGoings(context);
    }
}
