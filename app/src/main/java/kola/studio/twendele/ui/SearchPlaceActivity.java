package kola.studio.twendele.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import kola.studio.twendele.Adapters.PlaceAdapter;
import kola.studio.twendele.Adapters.SearchPlaceAdapter;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.Models.Place;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.Utilities.NetworkLocationProvider;

public class SearchPlaceActivity extends AppCompatActivity{
    private PlaceAdapter placeAdapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    AQuery aQuery;
    private View view;
    private MaterialSearchView searchView;
    private SearchPlaceAdapter searchPlaceAdapter;
    private PlaceViewModel placeViewModel;
    private NetworkLocationProvider networkLocationProvider;
    RecyclerView.LayoutManager layoutManager;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_place);
        bindViews();
        aQuery = new AQuery(this);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        progressBar.setIndeterminate(true);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);
        placeViewModel = ViewModelProviders.of(this).get(PlaceViewModel.class);
        placeViewModel.getData().observe(this, data -> {
            progressBar.setVisibility(View.INVISIBLE);
            if(data == null){
                Snackbar.make(view,"No places found",Snackbar.LENGTH_SHORT).show();
                finish();
            }else {
                displaySearchResult(data);
            }
        });
        placeViewModel.nearByPlaces().observe(this, places -> {
            progressBar.setVisibility(View.INVISIBLE);
            if(places == null){
                Snackbar.make(view,"No places found",Snackbar.LENGTH_SHORT).show();
                finish();
            }else {
                listNearbyPlaces(places);
            }
        });
        networkLocationProvider = new NetworkLocationProvider(this);
        getNearbyPlace();
    }

    private void bindViews(){
        view = findViewById(R.id.searchPlaceView);
        progressBar = findViewById(R.id.progress_circular);
        recyclerView = findViewById(R.id.recycler_view);
        toolbar = findViewById(R.id.toolbar);
    }

    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.search_results));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(view -> {
                aQuery.openFromLeft(GoingActivity.class);
                finish();
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_item, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = findViewById(R.id.searchviewBox);
        searchView.setMenuItem(item);
        searchView.setHint(getString(R.string.places));
        searchView.setHintTextColor(R.color.black);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                searchAPlace(query);
//                if(query != null  && !query.isEmpty()){
//
//                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String searchText) {
                //Do some magic
                if(searchText != null && !searchText.isEmpty()){
                    placeAdapter.getFilter().filter(searchText);
                }
                return true;
            }
        });
        return true;
    }

    private void displaySearchResult(ArrayList<Data> place) {
        if(place.size() <= 0){
            progressBar.setVisibility(View.VISIBLE);
            Snackbar.make(view,getResources().getString(R.string.results_msg),Snackbar.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
        }
        searchPlaceAdapter = new SearchPlaceAdapter(place,SearchPlaceActivity.this);
        recyclerView.setAdapter(searchPlaceAdapter);
    }

    private void listNearbyPlaces(ArrayList<Place> place) {
        if(place.size() <= 0){
            progressBar.setVisibility(View.VISIBLE);
            Snackbar.make(view,getResources().getString(R.string.results_msg),Snackbar.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
        }
        placeAdapter = new PlaceAdapter(place,SearchPlaceActivity.this);
        recyclerView.setAdapter(placeAdapter);
    }

    private void getNearbyPlace(){
        if(!Twendele.isDeviceOnline()){
            Snackbar.make(view,"No Internet connection",Snackbar.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        String latitude = String.valueOf(networkLocationProvider.getLocation()[0]);
        String longitude = String.valueOf(networkLocationProvider.getLocation()[1]);
        placeViewModel.getNearByPlaces(this,latitude,longitude);
    }

    private void searchAPlace(String query){
        if(!Twendele.isDeviceOnline()){
            Snackbar.make(view,"No Internet connection",Snackbar.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        String latitude = String.valueOf(networkLocationProvider.getLocation()[0]);
        String longitude = String.valueOf(networkLocationProvider.getLocation()[1]);
        placeViewModel.searchVenue(this,query,latitude,longitude);
    }
}
