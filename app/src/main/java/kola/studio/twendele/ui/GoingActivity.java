package kola.studio.twendele.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.aquery.AQuery;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.onurkagan.ksnack_lib.Animations.Fade;
import com.onurkagan.ksnack_lib.MinimalKSnack.MinimalKSnack;
import com.onurkagan.ksnack_lib.MinimalKSnack.MinimalKSnackStyle;

import org.michaelbel.bottomsheet.BottomSheet;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.Models.Goings;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.Utilities.Helper;
import kola.studio.twendele.Utilities.NetworkLocationProvider;
import kola.studio.twendele.Utilities.Utils;
import kola.studio.twendele.ui.Login.LoginActivity;
import kola.studio.twendele.ui.User.MyProfile;
import kola.studio.twendele.ui.User.UserViewModel;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class GoingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMyLocationChangeListener, EasyPermissions.PermissionCallbacks {

    private static final int RC_LOCATION_PERM = 101;
    DrawerLayout drawer;
    private boolean pressBackBtnOnce;
    AQuery aQuery;
    private GoogleMap mMap;
    Map<Marker, Data> markers;
    BottomSheet builder;
    final int LAUNCH_CREATE_GOING_INTENT_ACTIVTY=1;
    private UserViewModel userViewModel;
    private GoingsViewModel goingsViewModel;
    private NetworkLocationProvider networkLocationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        aQuery = new AQuery(this);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        float defaultValue = 0;
        String latitude = String.valueOf(sharedPreferences.getFloat(getString(R.string.latitude),defaultValue));
        String longitude = String.valueOf(sharedPreferences.getFloat(getString(R.string.longitude),defaultValue));

        FloatingActionButton floatingActionButton = findViewById(R.id.fabSearch);

        floatingActionButton.setOnClickListener(view -> {
            Intent intent = new Intent(GoingActivity.this, SearchPlaceActivity.class);
            intent.putExtra("latitude",latitude);
            intent.putExtra("longitude",longitude);
            aQuery.open(intent);
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        userViewModel.getUserDetails().observe(this, userDetails -> {
            if(userDetails == null){
                finish();
            }else {
                aQuery.id(R.id.profile_image).image(userDetails.getUrl());
                aQuery.id(R.id.uname).text(userDetails.getUserProfile().getFullName());
                aQuery.id(R.id.txt_mobile).text(userDetails.getUserProfile().getMobileNo());
            }
        });

        goingsViewModel = ViewModelProviders.of(this).get(GoingsViewModel.class);
//        fetchGoings();
        goingsViewModel.getFeed().observe(this, goings -> {
            if(goings == null){
                finish();
            }else{
                showGoings(goings);
            }
        });

        locationTask();
//        fetchGoings();
    }

    private void promptLogout(Context context) {
        new AlertDialog.Builder(context)
                .setTitle("Log out")
                .setMessage("Are you sure you want to log out ?")
                .setPositiveButton(R.string.yes, (dialog, which) -> logout())
                .setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void logout() {
        Helper.logout();
        aQuery.open(LoginActivity.class);
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        //searchbar
//        if(searchView.isSearchOpen()){
//            searchView.closeSearch();
//        }
        //navigation drawer
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!pressBackBtnOnce) {
            this.pressBackBtnOnce = true;
            Toasty.info(getApplicationContext(), "Press again to exit ", Toasty.LENGTH_SHORT, true).show();
            new Handler().postDelayed(() -> {
                this.pressBackBtnOnce = false;
            }, 2000);
        } else {
            super.onBackPressed();
            finishAffinity();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.home:
                startActivity(new Intent(this,GoingActivity.class));
                break;
            case R.id.profile:
                startActivity(new Intent(GoingActivity.this, MyProfile.class));
                break;
            case R.id.invite:
                startActivity(new Intent(GoingActivity.this, FindFriendsActivity.class));
                break;
            case R.id.logout_btn:
                promptLogout(this);
                break;
            case R.id.help:
                aQuery.open(HelpActivity.class);
                break;
            default:
                Toasty.info(getBaseContext(), "Unknown operation", Toast.LENGTH_SHORT, true).show();
                Utils.log("Unknown operation");
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showGoings(Goings goings) {
        List<Data> goingsList = goings.getData();
        markers = new HashMap<>();
        for (int i = 0; i < goingsList.size(); i++) {
            Data data = goingsList.get(i);
            double lat = data.getLatitude();
            double lng = data.getLongitude();
            String venue = data.getName();
            String imageUri = data.getCategories().get(0).getIcon();
            LatLng location = new LatLng(lat, lng);
            Marker marker = null;
                marker = mMap.addMarker(new MarkerOptions().position(location)
                        .title(venue));
            assert marker != null;
            marker.setTag(i);
//            marker.setIcon(BitmapDescriptorFactory.fromBitmap(convertToBitmap(imageUri)));
            markers.put(marker, data);
            Utils.log("marker image: "+convertToBitmap(imageUri));
        }
    }

    private Bitmap convertToBitmap(String imageUri) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            URL url = new URL(imageUri);
            return BitmapFactory.decodeStream((InputStream)url.getContent());
//            Bitmap bitmap = BitmapFactory.decodeStream((InputStream)url.getContent());
//            Bitmap bitmap1 = bitmap.copy(Bitmap.Config.ARGB_8888,true);
//            return bitmap1;
        } catch (IOException e) {
            Log.e("TAG", e.getMessage());
            return null;
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        MapStyleOptions mapStyleOptions = MapStyleOptions.loadRawResourceStyle(this,R.raw.nightthemestyle);
        mMap.setMyLocationEnabled(true);
//        mMap.setMapStyle(mapStyleOptions);
        mMap.setOnMyLocationChangeListener(this);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) { }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        int[] items = new int[]{R.string.check_out};
        int[] icons = new int[]{R.drawable.ic_keyboard_arrow_right_black_24dp};
        builder = new BottomSheet.Builder(this)
                .setTitle(markers.get(marker).getName())
                .setTitleMultiline(true)
                .setFullWidth(true)
                .setWindowDimming(170)
                .setTitleTextColor(getResources().getColor(R.color.orange))
                .setItems(items, icons, (dialogInterface, i) -> {
                    Intent intent = new Intent(GoingActivity.this, CreateGoingIntentActivity.class);
                    intent.putExtra("DATA", markers.get(marker));
                    startActivityForResult(intent,LAUNCH_CREATE_GOING_INTENT_ACTIVTY);
                })
                .setItemTextColor(getResources().getColor(R.color.black))
                .setOnShowListener(dialogInterface -> {

                })
                .setDividers(true)
                .setOnDismissListener(dialogInterface -> dialogInterface.dismiss())
                .show();
        return false;
    }

    @Override
    public void onMyLocationChange(Location location) {
        Utils.log("my location lat/lng: "+location.getLatitude()+":"+location.getLongitude());
        LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());
//        mMap.setMinZoomPreference(10.0f);
//        mMap.setMaxZoomPreference(18.0f);
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation,mMap.));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation,14));
    }
//    @Override
//    public void onMyLocationChange(Location location) {
//        Utils.log("my location lat/lng: "+location.getLatitude()+":"+location.getLongitude());
////        getNearByPlaces(Double.toString(location.getLatitude()),Double.toString(location.getLongitude()));
//
//        if(location == null){
//            return;
//        }
//        LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());
////        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(userLocation);
////        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);
////        mMap.moveCamera(cameraUpdate);
////        mMap.animateCamera(zoom);
//        Toasty.info(this,"lat/lng"+location.getLatitude()+":"+location.getLongitude(),Toasty.LENGTH_SHORT).show();
//
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(userLocation));
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation,14));
//    }

    @Override
    protected void onStart() {
        super.onStart();
        fetchUserDetails();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchUserDetails();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LAUNCH_CREATE_GOING_INTENT_ACTIVTY){
            if(resultCode == Activity.RESULT_OK){
                displayNotification(data.getStringExtra("STATUS"));
            }
        }
    }

    private void displayNotification(String status){
        MinimalKSnack minimalKSnack = new MinimalKSnack(GoingActivity.this);
        minimalKSnack.setMessage(status); // message
        minimalKSnack.setStyle(MinimalKSnackStyle.STYLE_SUCCESS); // style
        minimalKSnack.setBackgroundColor(R.color.metallic_blue); // background color
        minimalKSnack.setAnimation(Fade.In.getAnimation(),Fade.Out.getAnimation()); // show and hide animations
        minimalKSnack.setDuration(2000); // you can use for auto close.
        View view = minimalKSnack.getMinimalSnackView();
        LinearLayout.LayoutParams params =(LinearLayout.LayoutParams)view.getLayoutParams();
        params.gravity =  Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        TypedValue typedValue = new TypedValue();
        int actionBarHeight =0;
        if(getTheme().resolveAttribute(R.attr.actionBarSize,typedValue,true)){
            actionBarHeight = TypedValue.complexToDimensionPixelOffset(typedValue.data,getResources().getDisplayMetrics());
        }
        params.setMargins(0,actionBarHeight+46,0,0);
        view.setLayoutParams(params);
        minimalKSnack.show();
    }

    private void fetchUserDetails(){
        if(!Twendele.isDeviceOnline()){
            return;
        }
        userViewModel.getUserProfile(this);
    }


    private void animateMarker(Marker userpositionMarker,final Location location) {
        final Interpolator interpolator = new BounceInterpolator();
        final long startTime = SystemClock.uptimeMillis();
        final long duration = 50;
//        final long duration = 2000;
        final double startRotation = userpositionMarker.getRotation();
        final Handler handler = new Handler();
        final LatLng startUserLocation = userpositionMarker.getPosition();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        long elapsedTime = SystemClock.uptimeMillis() - startTime;
                        float t = interpolator.getInterpolation((float) elapsedTime/duration);
                        double latitude = t*location.getLatitude()+(1-t)*startUserLocation.latitude;
                        double longitude = t*location.getLongitude()+(1-t)*startUserLocation.longitude;
                        float rotation = (float) (t*location.getBearing() + (1-t)* startRotation);
                        userpositionMarker.setPosition(new LatLng(latitude,longitude));
                        userpositionMarker.setRotation(rotation);
                        if(t < 1.0){
                            handler.postDelayed(this,16);
                        }
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

    private void fetchGoings(){
        if(!Twendele.isDeviceOnline()){
            Helper.showAlert(this,"Alert","No internet connection");
            return;
        }

        if(!networkLocationProvider.isLocationEnabled()){
            Helper.showAlertLocation(this,"Location disabled","Twendele will not work until you've switched on your location.");
        }else{
            goingsViewModel.feed(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms))
        {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private boolean hasLocationPermission(){
        return EasyPermissions.hasPermissions(this,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public void locationTask(){
        networkLocationProvider = new NetworkLocationProvider(this);
        if(!hasLocationPermission()){
            fetchGoings();
        }else{
            Toasty.info(this,"Twendele needs permission to access location.",Toasty.LENGTH_SHORT,true).show();
            EasyPermissions.requestPermissions(this,"Twendele needs permission to access location.",RC_LOCATION_PERM,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }
}
