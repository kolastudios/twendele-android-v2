package kola.studio.twendele.ui.Login;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.AccountManager;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.User;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kola.studio.twendele.Utilities.Helper.isPasswordValid;
import static kola.studio.twendele.Utilities.Helper.isPhoneNumberValid;

public class LoginViewModel extends ViewModel {
    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<User> user = new MutableLiveData<>();

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }
    LiveData<User> getUser() {
        return user;
    }

    public void login(Context context, String phoneNumber, String password){
        ApiInterface apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<User> call = apiInterface.login(phoneNumber,password);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    User authUser = response.body();
                    if(authUser != null){
                        authUser.save();
                        Gson gson = new Gson();
                        String jsonData = gson.toJson(authUser);
                        Prefs.putString(AccountManager.TOKEN, authUser.getToken());
                        Prefs.putString(AccountManager.MOBILE, authUser.getMobileNo());
                        Prefs.putString(AccountManager.AUTH_USER, jsonData);
                        user.setValue(authUser);
                    }else {
                        user.setValue(null);
                    }
                }else{
                    Utils.log("login response message -> "+response.body());
                    Utils.log("login response code -> "+response.code());
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Utils.log("response message -> "+t.getMessage());
                user.setValue(null);
            }
        });
    }

    public void loginDataChanged(String phoneNumber, String password) {
        if (!isPhoneNumberValid(phoneNumber)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_phoneNo, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }
}
