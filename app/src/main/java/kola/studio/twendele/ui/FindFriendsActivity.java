package kola.studio.twendele.ui;

import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import kola.studio.twendele.Adapters.TabAdapter;
import kola.studio.twendele.Fragments.FindFriends;
import kola.studio.twendele.Fragments.FriendRequests;
import kola.studio.twendele.Fragments.FriendsListFragment;
import kola.studio.twendele.R;

public class FindFriendsActivity extends AppCompatActivity {

    private TabAdapter tabAdapter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);
        ViewPager viewPager = findViewById(R.id.view_pager);
        toolbar = findViewById(R.id.toolbar);
        TabLayout tabs = findViewById(R.id.tabs);
        tabAdapter = new TabAdapter(getSupportFragmentManager());
        tabAdapter.addFragment(new FindFriends(),getResources().getString(R.string.people));
        tabAdapter.addFragment(new FriendRequests(),getResources().getString(R.string.friend_request));
        tabAdapter.addFragment(new FriendsListFragment(),getResources().getString(R.string.friends));
        tabs.setupWithViewPager(viewPager);
        viewPager.setAdapter(tabAdapter);
        tabs.setupWithViewPager(viewPager);
        toolbarSetUp(toolbar);
    }

    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.find_friends));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(v -> finish());
        }
    }
}