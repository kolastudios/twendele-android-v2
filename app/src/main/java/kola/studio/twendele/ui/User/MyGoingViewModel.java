package kola.studio.twendele.ui.User;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.Goings;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyGoingViewModel extends ViewModel {
    private MutableLiveData<Goings> goingsList = new MutableLiveData<>();

    LiveData<Goings> getGoings() { return goingsList; }

    public void myGoings(Context context){
        ApiInterface apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<Goings> call = apiInterface.myFeed();
        call.enqueue(new Callback<Goings>() {
            @Override
            public void onResponse(Call<Goings> call, Response<Goings> response) {
                Utils.log("my goings response body->"+response.body());
                Utils.log("response code->"+response.code());
                Goings goings = response.body();
                if(goings != null){
                    goingsList.setValue(goings);
                }else{
                    goingsList.setValue(null);
                    Utils.log("Failed to load goings."+response.errorBody());
                }
            }
            @Override
            public void onFailure(Call<Goings> call, Throwable t) {
                goingsList.setValue(null);
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }

}