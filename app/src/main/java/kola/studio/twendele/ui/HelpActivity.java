package kola.studio.twendele.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aquery.AQuery;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.Adapters.ListAdapter;
import kola.studio.twendele.Models.ListItems;
import kola.studio.twendele.R;

public class HelpActivity extends AppCompatActivity {

    ListAdapter listAdapter;
    Toolbar toolbar;
    AQuery aQuery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);
        aQuery = new AQuery(this);
        final ArrayList<ListItems> arrayList = new ArrayList<ListItems>();
        arrayList.add(new ListItems(getString(R.string.help)));
        arrayList.add(new ListItems(getString(R.string.about)));
        ListAdapter listAdapter = new ListAdapter(this,arrayList);

        ListView listView = findViewById(R.id.list_view);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener((adapterView, view, position, l) -> {
            switch (position){
                case 0:
                    Toasty.info(this,"Help Activity",Toasty.LENGTH_SHORT,true).show();
                    break;
                case 1:
                    aQuery.openFromRight(About.class);
                    break;
            }
        });
    }

    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.help_title));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(v -> finish());
        }
    }
}
