package kola.studio.twendele.ui.User;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.AddHeader;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.Utilities.Utils;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.widget.Toast.LENGTH_SHORT;

public class ActivityHeaderContent extends AppCompatActivity {
    private AQuery aQuery;
    private Uri resultUri;
    private ApiInterface apiInterface;
    private Toolbar toolbar;
    EditText headerContent;
    private UserViewModel userViewModel;
    private View rootView;
    private ImageView imageView;
    private Button updateBtn;
    private ProgressBar progressBar;
    private ImageView clickImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_header_content);
        toolbar = findViewById(R.id.toolbar);
        rootView = findViewById(R.id.add_header_content);
        progressBar = findViewById(R.id.circular_progress_bar);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);
        bindViews();
        aQuery = new AQuery(this);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        userViewModel.getHeader1().observe(this, new Observer<AddHeader>() {
            @Override
            public void onChanged(AddHeader addHeader) {
                progressBar.setVisibility(View.INVISIBLE);
                if(addHeader == null){
                    finish();
                }else {
                    aQuery.openFromLeft(MyProfile.class);
                    finish();
                }
            }
        });
        aQuery.view(imageView).click(view -> {
            choosePhoto();
        });
        aQuery.view(updateBtn).click(view -> {
            addHeaderContent();
        });

//        aQuery.id(R.id.header_bkgnd).click(view -> {
//            choosePhoto();
//        });
//        aQuery.id(R.id.save_btn).click(view -> {
//            addHeaderContent();
//        });

    }
    private void bindViews(){
        imageView = findViewById(R.id.header_bkgnd);
        updateBtn = findViewById(R.id.save_btn);
        headerContent = findViewById(R.id.header_content);
    }
    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Edit bio");
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(view -> {
                aQuery.openFromLeft(MyProfile.class);
                finish();
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            try{
                resultUri = result.getUri();
                aQuery.id(R.id.header_bkgnd).image(resultUri);
                clickImageView = findViewById(R.id.clickupload);
                clickImageView.setVisibility(View.INVISIBLE);
            } catch (Exception e){
                e.printStackTrace();
                clickImageView = findViewById(R.id.clickupload);
                clickImageView.setVisibility(View.VISIBLE);
                Toasty.info(this,"No option selected !", LENGTH_SHORT,true).show();
            }
        }
    }

    private void addHeaderContent(){
        if(!Twendele.isDeviceOnline()){
            Snackbar.make(rootView,"No Internet connection",Snackbar.LENGTH_SHORT).show();
            return;
        }
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        try {
            File file = new File(resultUri.getPath());
            Utils.log("image uri path -> " + file.exists() + " === path " + file.getPath());
            RequestBody requestFile = RequestBody.create(null, file);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            userViewModel.addHeaderContent(this,headerContent.getText().toString(),filePart);
        }catch (Exception e){
            e.printStackTrace();
            Toasty.error(ActivityHeaderContent.this, "Error occured while uploading header content",
                    Toasty.LENGTH_SHORT, true).show();
        }
    }

    private void choosePhoto() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(800, 400)
                .start(this);
    }
}