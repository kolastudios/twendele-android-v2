package kola.studio.twendele.ui.VerifyPhoneNumber;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.VerifyPhoneNo;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneVerificationViewModel extends ViewModel {

    private  MutableLiveData<VerifyPhoneNo> verifyPhoneNo = new MutableLiveData<>();

    LiveData<VerifyPhoneNo> getPhoneNumber() {
        return verifyPhoneNo;
    }

    public void verifyMobileNo(Context context,String otpCode, String mobileNumber){
        ApiInterface apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Utils.log("Mobile number -> "+mobileNumber);
        Call<VerifyPhoneNo> call = apiInterface.verifyPhoneNo(otpCode,mobileNumber);
        call.enqueue(new Callback<VerifyPhoneNo>() {
            @Override
            public void onResponse(Call<VerifyPhoneNo> call, Response<VerifyPhoneNo> response) {
                Utils.log("Response code -> " + response.code());
                if(response.isSuccessful()){
                    VerifyPhoneNo otpVerification = response.body();
                    if(otpVerification != null){
                        otpVerification.save();
                        verifyPhoneNo.setValue(otpVerification);
                    }else{
                        verifyPhoneNo.setValue(null);
                    }
                }else{
                    verifyPhoneNo.setValue(null);
                    Utils.log("Failed to send details to server"+response.errorBody());
                }
            }
            @Override
            public void onFailure(Call<VerifyPhoneNo> call, Throwable t) {
                verifyPhoneNo.setValue(null);
                Utils.log("Failed to send details to server"+t.getMessage());
            }
        });
    }
}
