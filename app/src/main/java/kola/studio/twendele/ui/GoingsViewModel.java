package kola.studio.twendele.ui;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.Goings;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoingsViewModel extends ViewModel {

    private MutableLiveData<Goings> feed = new MutableLiveData<>();

    LiveData<Goings> getFeed() { return feed; }

    public void feed(Context context) {
        ApiInterface apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<Goings> call = apiInterface.feed();
        call.enqueue(new Callback<Goings>() {
            @Override
            public void onResponse(Call<Goings> call, Response<Goings> response) {
                Utils.log("goings response " + response.body());
                Goings goings = response.body();
                if(goings != null){
                    feed.setValue(goings);
                }else{
                    feed.setValue(null);
                }
            }
            @Override
            public void onFailure(Call<Goings> call, Throwable t) {
                feed.setValue(null);
                Utils.log("Error connecting to server" + t.getMessage());
            }
        });
    }
}
