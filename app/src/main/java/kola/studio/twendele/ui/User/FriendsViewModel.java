package kola.studio.twendele.ui.User;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.FriendList;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendsViewModel extends ViewModel {

    private MutableLiveData<ArrayList<FriendList>> friendList = new MutableLiveData<>();

    LiveData<ArrayList<FriendList>> getFriends() { return friendList; }

    public void getUsersFriends(Context context){
        ApiInterface apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<ArrayList<FriendList>> call = apiInterface.getListOfFriends();
        call.enqueue(new Callback<ArrayList<FriendList>>() {
            @Override
            public void onResponse(Call<ArrayList<FriendList>> call, Response<ArrayList<FriendList>> response) {
                Utils.log("responseMessage->"+response.message());
                Utils.log("responseBody->"+response.message());
                Utils.log("responseCode->"+response.code());
                ArrayList<FriendList> friends = response.body();
                if(friends != null){
                    friendList.setValue(friends);
                }else{
                    friendList.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FriendList>> call, Throwable t) {
                friendList.setValue(null);
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }
}
