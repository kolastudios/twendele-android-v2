package kola.studio.twendele.ui.Register;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.Models.User;
import kola.studio.twendele.R;
import kola.studio.twendele.Twendele;
import kola.studio.twendele.Utilities.Helper;
import kola.studio.twendele.Utilities.Utils;
import kola.studio.twendele.ui.Login.LoginActivity;
import kola.studio.twendele.ui.VerifyPhoneNumber.MobileNoVerification;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static android.widget.Toast.LENGTH_SHORT;
import static com.facebook.internal.ImageDownloader.clearCache;

public class RegisterActivity extends AppCompatActivity  implements View.OnClickListener,EasyPermissions.PermissionCallbacks{

    private int RC_CAMERA_PERM = 100;

    private Toolbar toolbar;
    private AQuery aq;
    EditText mobile,dateOfBirth,name,password,homeAddress;
    private RadioGroup radioGroup;
    private Button submit_data;
    ImageView imgProfile;
    CountryCodePicker countryCodePicker;
    DatePickerDialog datePickerDialog;
    private KProgressHUD kProgressHUD;
    private RadioButton gender;
    private Uri resultUri;
    private String imagePath;
    private View rootView;
    private RegisterViewModel registerViewModel;
    private ImageView clickUpload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarSetUp(toolbar);
        aq = new AQuery(this);
        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        bindViews();
        setViewActions();
        showDatePickerDialog();
        clearCache(this);
        registerViewModel.getRegisterFormState().observe(this, new Observer<RegisterFormState>() {
            @Override
            public void onChanged(RegisterFormState registerFormState) {
                if(registerFormState == null){
                    return;
                }
                submit_data.setEnabled(registerFormState.isDataValid());
                if(registerFormState.getNameError() != null){
                    name.setError(getString(registerFormState.getNameError()));
                }
                if(registerFormState.getMobileNumberError() != null){
                    mobile.setError(getString(registerFormState.getMobileNumberError()));
                }
                if(registerFormState.getHomeAddressError() != null){
                    homeAddress.setError(getString(registerFormState.getHomeAddressError()));
                }
                if(registerFormState.getDateOfBirthError() != null){
                    dateOfBirth.setError(getString(registerFormState.getDateOfBirthError()));
                    Utils.log("date of birth -> "+ dateOfBirth.getText().toString());
                }
                if(registerFormState.getPasswordError() != null){
                    password.setError(getString(registerFormState.getPasswordError()));
                }
            }
        });

        registerViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                kProgressHUD.dismiss();
                if(user == null){
                    Snackbar.make(rootView,"This account already already exists!",Snackbar.LENGTH_SHORT).show();
                }else{
                    Snackbar.make(rootView,"Registration Successful.",Snackbar.LENGTH_SHORT).show();
                    verifyPhoneNo();
                }
            }
        });

       aq.view(submit_data).click(view -> registerUser());
    }

    private void verifyPhoneNo() {
        aq.openFromRight(MobileNoVerification.class);
        finish();
    }

    private void setViewActions(){
        dateOfBirth.setOnClickListener(this);
        submit_data.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
    }

    private void showDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(RegisterActivity.this,android.R.style.Theme_Holo_Light_Dialog, (view, year, month, dayOfMonth) -> {
            dateOfBirth.setText(year+"-"+(month+1)+"-"+dayOfMonth);
            datePickerDialog.dismiss();
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void toolbarSetUp(Toolbar toolbar) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_register));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(view -> {
                aq.openFromLeft(LoginActivity.class);
                finish();
            });
        }
    }

    public void bindViews(){
        homeAddress = findViewById(R.id.address);
        name = findViewById(R.id.full_names);
        rootView = findViewById(R.id.register_user);
        mobile = findViewById(R.id.mobile_no);
        password = findViewById(R.id.password);
        dateOfBirth = findViewById(R.id.dob);
        radioGroup = findViewById(R.id.gender);
        imgProfile = findViewById(R.id.profile_photo);
        countryCodePicker = findViewById(R.id.ccp);
        countryCodePicker.registerCarrierNumberEditText(mobile);
        submit_data = findViewById(R.id.register_btn);
        clickUpload = findViewById(R.id.click_upload);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                registerViewModel.registerDataChanged(name.getText().toString(),homeAddress.getText().toString(),
                        dateOfBirth.getText().toString(),countryCodePicker.getFullNumber(),password.getText().toString());
            }
        };
        name.addTextChangedListener(textWatcher);
        dateOfBirth.addTextChangedListener(textWatcher);
        homeAddress.addTextChangedListener(textWatcher);
        password.addTextChangedListener(textWatcher);
        mobile.addTextChangedListener(textWatcher);
    }

    private void choosePhoto(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(800,800)
                .start(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dob:
                datePickerDialog.show();
                break;
            case R.id.profile_photo:
                cameraTask();
                break;
        }
    }

    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            try{
                resultUri = result.getUri();
                imagePath = resultUri.getPath();
                imgProfile.setImageURI(resultUri);
                clickUpload.setColorFilter(getResources().getColor(R.color.grey));
            } catch (Exception e){
                e.printStackTrace();
                clickUpload.setVisibility(View.VISIBLE);
                Toasty.info(this,"No option selected !", LENGTH_SHORT,true).show();
            }
        }
        //check for camera permission
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE)
        {
            String yes = getString(R.string.yes);
            String no = getString(R.string.no);
            String statusMessage = hasCameraPermission() ? yes:no;
            // Do something after user returned from app settings screen, like showing a Toast.
            if(statusMessage.equals(yes)){
                imgProfile.setEnabled(true);
            }else{
//                Toasty.info(this,("Camera permission "+statusMessage+" granted"),Toasty.LENGTH_SHORT).show();
            }
        }
    }

    public void registerUser(){
        if(!Twendele.isDeviceOnline()){
            Helper.showAlert(this,"Alert","No internet connection");
            return;
        }
        kProgressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Registration in progress")
                .setCancellable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.8f)
                .show();
        int selectedGender = radioGroup.getCheckedRadioButtonId();
        gender = findViewById(selectedGender);
        Utils.log("gender value "+selectedGender);
        Utils.log("gender result "+gender);

        if(selectedGender < 1){
            kProgressHUD.dismiss();
            Snackbar.make(rootView,"Select your gender!",Snackbar.LENGTH_SHORT).show();
            return;
        }
        try {
            File file = new File(resultUri.getPath());
            Utils.log("image uri path -> " + file.exists() + " === path "+ file.getPath());
            RequestBody requestFile = RequestBody.create(null, file);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            Utils.log("date of birth result "+dateOfBirth.getText().toString());
            registerViewModel.register(this,name.getText().toString(),
                    countryCodePicker.getFullNumber(),password.getText().toString(),
                    homeAddress.getText().toString(),dateOfBirth.getText().toString(),
                    gender.getText().toString(),filePart);
        }catch (Exception e){
            kProgressHUD.dismiss();
            Snackbar.make(rootView,"Upload your profile picture",Snackbar.LENGTH_SHORT).show();
            Utils.log("Failed to retrieve image file"+ e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        imgProfile.setEnabled(true);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private boolean hasCameraPermission(){
        return EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA);
    }

    private void cameraTask(){
        if(hasCameraPermission()){
            imgProfile.setEnabled(true);
            imgProfile.setOnClickListener(view -> choosePhoto());
        }else{
            EasyPermissions.requestPermissions(this,"Twendele needs permission to access camera and media.",RC_CAMERA_PERM, Manifest.permission.CAMERA);
        }
    }
}
