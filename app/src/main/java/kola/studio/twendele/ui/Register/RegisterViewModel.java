package kola.studio.twendele.ui.Register;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.AccountManager;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.User;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kola.studio.twendele.Utilities.Helper.isDateValid;
import static kola.studio.twendele.Utilities.Helper.isFieldValid;
import static kola.studio.twendele.Utilities.Helper.isPasswordValid;
import static kola.studio.twendele.Utilities.Helper.isPhoneNumberValid;

public class RegisterViewModel extends ViewModel {
    private MutableLiveData<RegisterFormState> registerFormState = new MutableLiveData<>();
    private MutableLiveData<User> user = new MutableLiveData<>();

    LiveData<RegisterFormState> getRegisterFormState() {
        return registerFormState;
    }

    LiveData<User> getUser() {
        return user;
    }

    public void register(Context context, String name, String mobile,
                         String password, String homeAddress,
                         String dateOfBirth, String gender,
                         MultipartBody.Part profilePicture){

        HashMap<String, String> data = new HashMap<>();
        data.put("date_of_birth", dateOfBirth);
        data.put("password",password);
        data.put("full_name",name);
        data.put("address",homeAddress);
        data.put("gender",gender);
        data.put("mobile_no",mobile);
        ApiInterface apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<User> call = apiInterface.registerUser(data, profilePicture);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Utils.log("response msg -> "+response.body());
                Utils.log("response msg code -> "+response.code());
                Utils.log("response msg error body -> "+response.errorBody());
                if(response.isSuccessful()){
                    Utils.log("response message -> "+response.body());
                    Utils.log("response message code -> "+response.code());
                    User user1 = response.body();
                    user1.save();
                    Prefs.putString(AccountManager.MOBILE, user1.getMobileNo());
                    user.setValue(user1);
                }else{
                    user.setValue(null);
                    Utils.log("response msg1 error code -> "+response.code());
                    Utils.log("response msg1 error code body -> "+response.errorBody());
                    Utils.log("response msg1 -> "+response.body());
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                user.setValue(null);
                Utils.log("Failed to send details to server"+t.getMessage());
            }
        });
    }

    public void registerDataChanged(String name,String homeAddress,String dateOfBirth,String phoneNumber, String password) {
        if (!isFieldValid(name)) {
            registerFormState.setValue(new RegisterFormState(null, null,R.string.name_error,null,null,null,null));
        }else if (!isPhoneNumberValid(phoneNumber)) {
            registerFormState.setValue(new RegisterFormState(R.string.invalid_phoneNo, null,null,null,null,null,null));
        }else if (!isFieldValid(homeAddress)) {
            registerFormState.setValue(new RegisterFormState(null, null,null,null,null,null,R.string.invalid_address));
        } else if (!isDateValid(dateOfBirth)) {
            registerFormState.setValue(new RegisterFormState(null, null,null,null,R.string.set_date,null,null));
        } else if (!isPasswordValid(password)) {
            registerFormState.setValue(new RegisterFormState(null, R.string.invalid_password,null,null,null,null,null));
        } else {
            registerFormState.setValue(new RegisterFormState(true));
        }
    }
}
