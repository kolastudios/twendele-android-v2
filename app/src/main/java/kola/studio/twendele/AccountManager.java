package kola.studio.twendele;

import kola.studio.twendele.Models.User;

public class AccountManager {
    public static final String ACCESS_TOKEN ="accessToken" ;
    public static String TOKEN = "token";
    public static String NAME = "name";
    public static String MOBILE = "mobile";
    public static final String AUTH_USER = "auth_user";
    public static User getProfile(){
        User user = User.first(User.class);
        if(user == null){
            user = new User();
        }
        return user;
    }

}
