package kola.studio.twendele.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;

import kola.studio.twendele.R;

public class ImageVenueAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<Integer> images;
    private LayoutInflater layoutInflater;
    @Override
    public int getCount() {
        return images.size();
    }

    public ImageVenueAdapter(Context context, ArrayList<Integer> images) {
        this.context = context;
        this.images = images;
    }

    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((View) object);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Object instantiateItem(ViewGroup container, int position){
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.image_venue,null);
        assert view != null;
        ImageView imageView = view.findViewById(R.id.venue_images);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setClipToOutline(true);
        imageView.setImageResource(images.get(position));
        ViewPager vp = (ViewPager) container;
        vp.addView(view,0);
        return view;
    }


    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    public void restoreState(Parcelable state, ClassLoader loader){

    }

    public Parcelable saveState(){
        return null;
    }
}
