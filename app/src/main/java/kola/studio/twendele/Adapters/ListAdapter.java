package kola.studio.twendele.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import kola.studio.twendele.Models.ListItems;
import kola.studio.twendele.R;

public class ListAdapter extends ArrayAdapter<ListItems> {
    public ListAdapter(@NonNull Context context, ArrayList<ListItems> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent){

        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item,parent,false);
        }
        ListItems listItems = getItem(position);
        TextView nameTextView = listItemView.findViewById(R.id.text_name);
        nameTextView.setText(listItems.getItem());

        return listItemView;
    }
}
