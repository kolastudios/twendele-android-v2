package kola.studio.twendele.Adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kola.studio.twendele.Models.Category;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;

public class GoingAdapter extends RecyclerView.Adapter<GoingAdapter.ViewHolder> {
    private Context context;
    private List<Data> listItems;

    public GoingAdapter(Context context,List<Data> listItems) {
        this.context = context;
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public  ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GoingAdapter.ViewHolder viewHolder, int position) {
        Data data = listItems.get(position);
        Utils.log("place_name -> "+data.getName());
        viewHolder.name.setText(data.getName());
        viewHolder.location.setText(data.getAddress());
        viewHolder.country.setText(data.getCountry());
        List<Category> categories =data.getCategories();
        if(categories.size() > 0 ){
            viewHolder.photo.setImageURI(Uri.parse(categories.get(0).getIcon()));
        }
    }


    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView name;
        public TextView country;
        public TextView location;
        public CircleImageView photo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            country = itemView.findViewById(R.id.country);
            location = itemView.findViewById(R.id.location);
            photo = itemView.findViewById(R.id.photo);
        }
    }
}
