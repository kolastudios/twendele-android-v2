package kola.studio.twendele.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aquery.AQuery;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kola.studio.twendele.Models.Category;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.Models.Place;
import kola.studio.twendele.R;
import kola.studio.twendele.ui.CreateGoingIntentActivity;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.MyViewHolder> implements Filterable {
    private List<Place> filteredPlaces;
    List<Place> places;
    Context context;
    AQuery aQuery;

    public PlaceAdapter(List<Place> places, Context context) {
        this.filteredPlaces = places;
        this.places = places;
        this.context = context;
        aQuery = new AQuery(this.context);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String characterString = charSequence.toString().trim();
                if (characterString.isEmpty()) {
                    filteredPlaces = places;
                } else {
                    List<Place> filteredList = new ArrayList<>();
                    for (Place place : places) {
                        if (place.getName().toLowerCase().contains(characterString.toLowerCase())
                                || place.getAddress().toLowerCase().contains(characterString.toLowerCase()) ) {
                            filteredList.add(place);
                        }
                        filteredPlaces = filteredList;
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredPlaces;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredPlaces = (List<Place>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public PlaceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.venues,parent,false);
        return new PlaceAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceAdapter.MyViewHolder holder, int position) {
        Place data = filteredPlaces.get(position);
        holder.name.setText(data.getName());
        holder.location.setText(data.getAddress());
        holder.country.setText(data.getCountry());
        List<Category> categories = data.getCategories();
        if(categories.size() > 0) {
            Picasso.get().load(Uri.parse(categories.get(0).getIcon())).into(holder.photo);
        }
        holder.cardView.setOnClickListener(view -> {
            Intent intent = new Intent(context, CreateGoingIntentActivity.class);
            Gson gson = new Gson();
            String json = gson.toJson(data);
            intent.putExtra("DATA",gson.fromJson(json, Data.class));
            aQuery.openFromRight(intent);
        });
    }

    @Override
    public int getItemCount() {
        return filteredPlaces.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,country,location;
        public ImageView photo;
        CardView cardView;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            country = itemView.findViewById(R.id.country);
            location = itemView.findViewById(R.id.location);
            photo = itemView.findViewById(R.id.photo);
            cardView = itemView.findViewById(R.id.cardView);

        }
    }
}
