package kola.studio.twendele.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.aquery.AQuery;

import java.util.Collections;

import kola.studio.twendele.Models.Data;
import kola.studio.twendele.R;

public class FriendsGoingAdapter extends ArrayAdapter<Data> {
    private final LayoutInflater layoutInflater;
    private final Context context;

    public FriendsGoingAdapter(@NonNull Context context, int resource, Data objects) {
        super(context, resource, Collections.singletonList(objects));
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public View getView(int position, View view, ViewGroup vg) {
        Data data = getItem(position);
        if (view == null) {
            view = layoutInflater.inflate(R.layout.friends_going_layout, null);
        }

        AQuery aq = new AQuery(view);
        assert data != null;
        aq.id(R.id.friend_name).text(data.getFriendsGoing().get(0).getFriendName());
        aq.id(R.id.photo).image(data.getFriendsGoing().get(0).getFriendProfilePicture());
        return view;
    }
}
