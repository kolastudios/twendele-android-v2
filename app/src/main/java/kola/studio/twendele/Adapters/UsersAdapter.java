package kola.studio.twendele.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aquery.AQuery;

import java.util.ArrayList;

import kola.studio.twendele.ui.CustomPopUpWindow;
import kola.studio.twendele.Fragments.FindFriends;
import kola.studio.twendele.Models.Users_;
import kola.studio.twendele.R;

public class UsersAdapter extends ArrayAdapter<Users_> {

    private final LayoutInflater layoutInflater;
    private final Context context;

    public UsersAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Users_> objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        Users_ data = getItem(position);
        if (view == null) {
            view = layoutInflater.inflate(R.layout.users_layout, null);
        }

        AQuery aq = new AQuery(view);
        aq.id(R.id.users_name).text(data.getPeople().getFullName());
        aq.id(R.id.location).text(data.getPeople().getAddress());
        View finalView = view;
        aq.id(R.id.photo).click(view14 -> {
            new CustomPopUpWindow(finalView,context).popUpWindowForSendRequest(data);
        });
        aq.id(R.id.sendRequest).click(view12 -> {
            new FindFriends().sendFriendRequest(data.getPeople().getId(),context);
        });
//        aq.id(R.id. blockRequest).click(view13 -> {
//            new FindFriends().blockUser(data.getPeople().getId(),context);
//        });
        return view;
    }
}
