package kola.studio.twendele.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aquery.AQuery;

import java.util.List;

import kola.studio.twendele.ui.CustomPopUpWindow;
import kola.studio.twendele.Fragments.FriendRequests;
import kola.studio.twendele.Models.PendingFriendRequests;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;

public class FriendRequestAdapter extends ArrayAdapter<PendingFriendRequests> {
    private final LayoutInflater layoutInflater;
    private final Context context;

    public FriendRequestAdapter(@NonNull Context context, int resource, @NonNull List<PendingFriendRequests> objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        PendingFriendRequests data = getItem(position);
        if (view == null) {
            view = layoutInflater.inflate(R.layout.friend_request_layout, null);
        }

        AQuery aq = new AQuery(view);
        aq.id(R.id.friend_name).text(data.getSender().getFullName());
        aq.id(R.id.location).text(data.getSender().getAddress());
        View finalView = view;
        aq.id(R.id.photo).click(view14 -> {
            new CustomPopUpWindow(finalView,context).popUpWindowForFriendRequest(data);
        });
        aq.id(R.id.acceptBtn).click(view12 -> {
            Utils.log("Accept friend id "+ data.getSender().getId());
            new FriendRequests().accptFriendRequest(data.getSender().getId(),context);
        });
        aq.id(R.id.rejectBtn).click(view13 -> {
            Utils.log("Reject friend id "+ data.getSender().getId());
            new FriendRequests().rejectRequest(data.getSender().getId(),context);
        });
        return view;
    }
}
