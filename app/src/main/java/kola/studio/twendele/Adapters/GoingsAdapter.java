package kola.studio.twendele.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.aquery.AQuery;

import java.util.ArrayList;
import java.util.List;

import kola.studio.twendele.Models.Category;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;

public class GoingsAdapter extends ArrayAdapter<Data> {
    private ArrayList<Data> places;
    private LayoutInflater layoutInflater;

    public GoingsAdapter(@NonNull Context context, int resource, @NonNull List<Data> objects) {
        super(context, resource, objects);
        layoutInflater = LayoutInflater.from(context);
    }

    public View getView(int position, View view, ViewGroup vg) {
        Data data = getItem(position);
        Utils.log("Populating -> " + data.getName());
        if (view == null) {
            view = layoutInflater.inflate(R.layout.venues, null);
        }

        AQuery aq = new AQuery(view);
        aq.id(R.id.name).text(data.getName());
        aq.id(R.id.country).text(data.getCountry());
        aq.id(R.id.location).text(data.getAddress());
        List<Category> categories = data.getCategories();
        if(categories.size() > 0){
            aq.id(R.id.photo).image(categories.get(0).getIcon());
        }
        return view;
    }
}
