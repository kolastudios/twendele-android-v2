package kola.studio.twendele.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aquery.AQuery;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import kola.studio.twendele.Models.Category;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.R;
import kola.studio.twendele.ui.CreateGoingIntentActivity;

public class SearchPlaceAdapter extends RecyclerView.Adapter<SearchPlaceAdapter.MyViewHolder> {
    ArrayList<Data> places;
    Context context;
    private AQuery aQuery;

    public SearchPlaceAdapter(ArrayList<Data> places,Context context) {
        this.places = places;
        this.context = context;
        aQuery = new AQuery(this.context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.venues,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Data data = places.get(position);
        holder.name.setText(data.getName());
        holder.location.setText(data.getAddress());
        holder.country.setText(data.getCountry());
        List<Category> categories = data.getCategories();
        if(categories.size() > 0){
            Picasso.get().load(Uri.parse(categories.get(0).getIcon())).into(holder.photo);
        }

        holder.cardView.setOnClickListener(view -> {
            Intent intent = new Intent(context, CreateGoingIntentActivity.class);
            intent.putExtra("DATA",data);
            aQuery.openFromRight(intent);
        });
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView name,country,location;
        ImageView photo;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            country = itemView.findViewById(R.id.country);
            location = itemView.findViewById(R.id.location);
            photo = itemView.findViewById(R.id.photo);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
