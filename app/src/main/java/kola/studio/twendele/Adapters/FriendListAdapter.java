package kola.studio.twendele.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.aquery.AQuery;

import java.util.ArrayList;

import kola.studio.twendele.ui.CustomPopUpWindow;
import kola.studio.twendele.Models.Data;
import kola.studio.twendele.Models.FriendList;
import kola.studio.twendele.R;

public class FriendListAdapter extends ArrayAdapter<FriendList>{
    private ArrayList<Data> places;
    private LayoutInflater layoutInflater;
    private Context context;

    public FriendListAdapter(@NonNull Context context, int resource,ArrayList<FriendList> friendList) {
        super(context, resource, friendList);
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public View getView(int position, View view, ViewGroup vg) {
        FriendList data = getItem(position);
        if (view == null) {
            view = layoutInflater.inflate(R.layout.friends_list, null);
        }
        AQuery aq = new AQuery(view);
        aq.id(R.id.friend_name).text(data.getFriend().getFullName());
        aq.id(R.id.location).text(data.getFriend().getAddress());
        View finalView = view;
        aq.id(R.id.photo).image(data.getUrl()).click(view1 -> {
            new CustomPopUpWindow(finalView,context).popUpWindowForListView(data);
        });
        return view;
    }
}
