package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Header extends SugarRecord {

  @SerializedName("header_content")
  @Expose
  private HeaderContent headerContent;
  @SerializedName("url")
  @Expose
  private String url;

  public HeaderContent getHeaderContent() {
    return headerContent;
  }

  public void setHeaderContent(HeaderContent headerContent) {
    this.headerContent = headerContent;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

}