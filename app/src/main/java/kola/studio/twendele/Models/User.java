package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class User extends SugarRecord {

  @SerializedName("full_name")
  @Expose
  private String fullName;
  @SerializedName("mobile_no")
  @Expose
  private String mobileNo;
  @SerializedName("address")
  @Expose
  private String address;
  @SerializedName("date_of_birth")
  @Expose
  private String dateOfBirth;
  @SerializedName("gender")
  @Expose
  private String gender;
  @SerializedName("otp_code")
  @Expose
  private String otpCode;
  @SerializedName("updated_at")
  @Expose
  private String updatedAt;
  @SerializedName("created_at")
  @Expose
  private String createdAt;
  @SerializedName("token")
  @Expose
  private String token;

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getMobileNo() {
    return mobileNo;
  }

  public void setMobileNo(String mobileNo) {
    this.mobileNo = mobileNo;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getOtpCode() {
    return otpCode;
  }

  public void setOtpCode(String otpCode) {
    this.otpCode = otpCode;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

}