package kola.studio.twendele.Models;

public class AccountManager {
  public static final String AUTH_USER = "auth_user";
  public static String TOKEN = "token";
  public static String NAME = "name";
  public static String EMAIL = "email";
  public static String VERIFICATION_CODE = "verification_code";

  public static User getProfile(){
    User user = User.first(User.class);
    if(user == null){
      user = new User();
    }
    return user;
  }

}
