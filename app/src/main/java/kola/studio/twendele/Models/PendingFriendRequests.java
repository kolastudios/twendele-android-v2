package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class PendingFriendRequests extends SugarRecord implements Serializable {

    @SerializedName("sender")
    @Expose
    private Friend sender;
    @SerializedName("url")
    @Expose
    private String url;

    public Friend getSender() {
        return sender;
    }

    public void setSender(Friend sender) {
        this.sender = sender;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
