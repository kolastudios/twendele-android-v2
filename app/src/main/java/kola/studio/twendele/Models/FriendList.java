package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class FriendList extends SugarRecord implements Serializable {

  @SerializedName("friend")
  @Expose
  private Friend friend;
  @SerializedName("url")
  @Expose
  private String url;
  @SerializedName("number_of_friends")
  @Expose
  private Integer numberOfFriends;

  public Friend getFriend() {
    return friend;
  }

  public void setFriend(Friend friend) {
    this.friend = friend;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getNumberOfFriends() {
    return numberOfFriends;
  }

  public void setNumberOfFriends(Integer numberOfFriends) {
    this.numberOfFriends = numberOfFriends;
  }
}