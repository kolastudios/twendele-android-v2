
package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class Users_ extends SugarRecord implements Serializable {
    @SerializedName("people")
    @Expose
    private User people;
    @SerializedName("url")
    @Expose
    private String url;

    public User getPeople() {
        return people;
    }

    public void setPeople(User people) {
        this.people = people;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}