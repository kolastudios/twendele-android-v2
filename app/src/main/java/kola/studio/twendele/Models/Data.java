package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.List;

public class Data extends SugarRecord implements Serializable {

  @SerializedName("place_id")
  @Expose
  private String placeId;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("latitude")
  @Expose
  private Double latitude;
  @SerializedName("longitude")
  @Expose
  private Double longitude;
  @SerializedName("address")
  @Expose
  private String address;
  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("categories")
  @Expose
  private List<Category> categories = null;
  @SerializedName("people_going")
  @Expose
  private Integer peopleGoing;
  @SerializedName("friends_going")
  @Expose
  private List<FriendsGoing> friendsGoing = null;

  public String getPlaceId() {
    return placeId;
  }

  public void setPlaceId(String placeId) {
    this.placeId = placeId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public List<Category> getCategories() {
    return categories;
  }

  public void setCategories(List<Category> categories) {
    this.categories = categories;
  }

  public Integer getPeopleGoing() {
    return peopleGoing;
  }

  public void setPeopleGoing(Integer peopleGoing) {
    this.peopleGoing = peopleGoing;
  }

  public List<FriendsGoing> getFriendsGoing() {
    return friendsGoing;
  }

  public void setFriendsGoing(List<FriendsGoing> friendsGoing) {
    this.friendsGoing = friendsGoing;
  }

}