package kola.studio.twendele.Models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Venue extends SugarRecord implements Serializable {

  @SerializedName("Place")
  @Expose
  private List<Place> place = null;

  public List<Place> getPlace() {
    return place;
  }

  public void setPlace(List<Place> place) {
    this.place = place;
  }

}