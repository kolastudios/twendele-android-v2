package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class FriendsGoing extends SugarRecord implements Serializable {

    @SerializedName("friend_name")
    @Expose
    private String friendName;
    @SerializedName("friend_profile_picture")
    @Expose
    private String friendProfilePicture;

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendProfilePicture() {
        return friendProfilePicture;
    }

    public void setFriendProfilePicture(String friendProfilePicture) {
        this.friendProfilePicture = friendProfilePicture;
    }
}