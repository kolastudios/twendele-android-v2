package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class Category extends SugarRecord implements Serializable {

//  @SerializedName("id")
//  @Expose
//  private Integer id;
  @SerializedName("category_id")
  @Expose
  private String categoryId;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("plural_name")
  @Expose
  private String pluralName;
  @SerializedName("short_name")
  @Expose
  private String shortName;
  @SerializedName("icon")
  @Expose
  private String icon;
  @SerializedName("created_at")
  @Expose
  private String createdAt;
  @SerializedName("updated_at")
  @Expose
  private String updatedAt;
  @SerializedName("pivot")
  @Expose
  private Pivot pivot;

//  public Integer getId() {
//    return id;
//  }
//
//  public void setId(Integer id) {
//    this.id = id;
//  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPluralName() {
    return pluralName;
  }

  public void setPluralName(String pluralName) {
    this.pluralName = pluralName;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Pivot getPivot() {
    return pivot;
  }

  public void setPivot(Pivot pivot) {
    this.pivot = pivot;
  }

}