package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;

public class Pivot extends SugarRecord implements Serializable {

  @SerializedName("place_id")
  @Expose
  private Integer placeId;
  @SerializedName("category_id")
  @Expose
  private Integer categoryId;

  public Integer getPlaceId() {
    return placeId;
  }

  public void setPlaceId(Integer placeId) {
    this.placeId = placeId;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

}