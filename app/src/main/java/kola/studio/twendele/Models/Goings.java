package kola.studio.twendele.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.util.List;

public class Goings extends SugarRecord {

  @SerializedName("data")
  @Expose
  private List<Data> data = null;
  @SerializedName("links")
  @Expose
  private Links links;
  @SerializedName("meta")
  @Expose
  private Meta meta;

  public List<Data> getData() {
    return data;
  }

  public void setData(List<Data> data) {
    this.data = data;
  }

  public Links getLinks() {
    return links;
  }

  public void setLinks(Links links) {
    this.links = links;
  }

  public Meta getMeta() {
    return meta;
  }

  public void setMeta(Meta meta) {
    this.meta = meta;
  }

}