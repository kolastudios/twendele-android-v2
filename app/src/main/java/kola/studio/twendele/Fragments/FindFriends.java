package kola.studio.twendele.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Objects;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.Adapters.UsersAdapter;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.FriendRequest;
import kola.studio.twendele.Models.Users_;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FindFriends extends Fragment {
    private ProgressBar progressBar;
    private AQuery aQuery;
    private ApiInterface apiInterface;
    private Context context;
    private UsersAdapter usersAdapter;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_find_friends2, container, false);
        aQuery = new AQuery(Objects.requireNonNull(getActivity()));
        progressBar = view.findViewById(R.id.progress_circular);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPeople();
    }

    private void sendFriendRequest(){

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() { super.onDetach(); }

    private void getPeople(){
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        Call<ArrayList<Users_>> call = apiInterface.getUsers();
        call.enqueue(new Callback<ArrayList<Users_>>() {
            @Override
            public void onResponse(Call<ArrayList<Users_>> call, Response<ArrayList<Users_>> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.INVISIBLE);
                    if(response.isSuccessful()){
                        if(response.body().size() > 0){
                            populateUsers(response.body());
                        }else{
                            Snackbar.make(getView(),"No people available",Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<ArrayList<Users_>> call, Throwable t) {
                progressBar.setVisibility(View.VISIBLE);
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }

    private void populateUsers(ArrayList<Users_> body) {
        usersAdapter = new UsersAdapter(context,R.layout.users_layout,body);
        listView = getView().findViewById(R.id.list_view1);
        listView.setAdapter(usersAdapter);
    }
    public void sendFriendRequest(long userId,Context context) {
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        FriendRequest FriendRequest = new FriendRequest();
        Call<FriendRequest> call = apiInterface.sendFriendRequest(userId);
        call.enqueue(new Callback<FriendRequest>() {
            @Override
            public void onResponse(Call<FriendRequest> call, Response<FriendRequest> response) {
                if(response.isSuccessful()){
                    FriendRequest.save();
                }
            }
            @Override
            public void onFailure(Call<FriendRequest> call, Throwable t) {
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }
}
