package kola.studio.twendele.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.Adapters.FriendListAdapter;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.FriendList;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendsListFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ListView listView;
    private FriendListAdapter friendsListAdapter;
    private Context mContext;
    private ProgressBar progressBar;
    private ApiInterface apiInterface;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_friends_list, container, false);
        listView = view.findViewById(R.id.list_view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getFriendsList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void friendsList(ArrayList<FriendList> friendList){
        friendsListAdapter = new FriendListAdapter(mContext,R.layout.fragment_friends_list,friendList);
        listView.setAdapter(friendsListAdapter);
        listView.setOnItemClickListener(this);
    }

    private void getFriendsList(){
        progressBar = view.findViewById(R.id.progress_circular);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        apiInterface = APIClient.getClient(mContext).create(ApiInterface.class);
        Call<ArrayList<FriendList>> call = apiInterface.getListOfFriends();
        call.enqueue(new Callback<ArrayList<FriendList>>() {
            @Override
            public void onResponse(Call<ArrayList<FriendList>> call, Response<ArrayList<FriendList>> response) {
                progressBar.setVisibility(View.INVISIBLE);
                if(response.isSuccessful()){
                    friendsList(response.body());
                }
            }
            @Override
            public void onFailure(Call<ArrayList<FriendList>> call, Throwable t) {
                progressBar.setVisibility(View.VISIBLE);
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });

    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
}
