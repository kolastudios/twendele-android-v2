package kola.studio.twendele.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aquery.AQuery;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Objects;

import kola.studio.twendele.APIClient;
import kola.studio.twendele.Adapters.FriendRequestAdapter;
import kola.studio.twendele.ApiInterface;
import kola.studio.twendele.Models.FriendRequestStatus;
import kola.studio.twendele.Models.PendingFriendRequests;
import kola.studio.twendele.R;
import kola.studio.twendele.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendRequests extends Fragment {
    private ProgressBar progressBar;
    private ApiInterface apiInterface;
    private Context mContext;
    private FriendRequestAdapter friendRequestAdapter;
    private ListView listView;
    private AQuery aQuery;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_requests, container, false);
        aQuery = new AQuery(Objects.requireNonNull(getActivity()));
        progressBar = view.findViewById(R.id.circular_progress_bar);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getFriendRequests();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }
    private void getFriendRequests(){
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        apiInterface = APIClient.getClient(mContext).create(ApiInterface.class);
        Call<ArrayList<PendingFriendRequests>> call = apiInterface.getPendingFriendRequests();
        call.enqueue(new Callback<ArrayList<PendingFriendRequests>>() {
            @Override
            public void onResponse(Call<ArrayList<PendingFriendRequests>> call, Response<ArrayList<PendingFriendRequests>> response) {
                progressBar.setVisibility(View.INVISIBLE);
                if(response.isSuccessful()){
                    assert response.body() != null;
                    if(response.body().size() > 0){
                        Utils.log("response content: "+response.body());
                        populateFriendRequest(response.body());
                    }else{
                        Snackbar.make(getView(),"No friend requests at the moment.",Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ArrayList<PendingFriendRequests>> call, Throwable t) {
                progressBar.setVisibility(View.VISIBLE);
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }

    private void populateFriendRequest(ArrayList<PendingFriendRequests> pendingFriendRequests) {
        friendRequestAdapter = new FriendRequestAdapter(mContext,R.layout.friend_request_layout,pendingFriendRequests);
        listView = getActivity().findViewById(R.id.list_view);
        listView.setAdapter(friendRequestAdapter);
    }

    public void accptFriendRequest(long friendId,Context context) {
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        FriendRequestStatus friendRequestStatus = new FriendRequestStatus();
        Call<FriendRequestStatus> call = apiInterface.acceptFriendRequest(friendId);
        call.enqueue(new Callback<FriendRequestStatus>() {
            @Override
            public void onResponse(Call<FriendRequestStatus> call, Response<FriendRequestStatus> response) {
                if(response.isSuccessful()){
                    friendRequestStatus.save();
                    Snackbar.make(Objects.requireNonNull(getView()),response.body().getMessage(),Snackbar.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<FriendRequestStatus> call, Throwable t) {
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }
    public void rejectRequest(long friendId,Context context){
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
        FriendRequestStatus friendRequestStatus = new FriendRequestStatus();
        Call<FriendRequestStatus> call = apiInterface.rejectFriendRequest(friendId);
        call.enqueue(new Callback<FriendRequestStatus>() {
            @Override
            public void onResponse(Call<FriendRequestStatus> call, Response<FriendRequestStatus> response) {
                if(response.isSuccessful()){
                    friendRequestStatus.save();
                    Snackbar.make(Objects.requireNonNull(getView()),response.body().getMessage(),Snackbar.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<FriendRequestStatus> call, Throwable t) {
                Utils.log("Error connecting to server"+t.getMessage());
            }
        });
    }
}
