package kola.studio.twendele;

public class Constants {
//    public static final String API_URL = "http://192.168.137.1:8000/api/";
    public static final String API_URL = "https://twendele.kolastudios.com/api/";
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_PRAGMA = "Pragma";
}
