package kola.studio.twendele.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import kola.studio.twendele.AccountManager;
import kola.studio.twendele.Models.User;
import kola.studio.twendele.R;

public class Helper {

    public static boolean isPhoneNumberValid(String mobileNumber) {
        if (mobileNumber == null) {
            return false;
        }
        return mobileNumber.trim().length() == 12;
    }

    public static boolean isPasswordValid(String field) {
        return field != null && field.trim().length() > 5;
    }
    public static boolean isFieldValid(String field) {
        if(field == null){
            return false;
        }
       return field.trim().length() > 5;
    }
    public static boolean isDateValid(String field) {
        if(field == null){
            return false;
        }
       return field.trim().length() > 0;
    }

    public static boolean isRadioBtnChecked(int selectedGender){
        return selectedGender == 0;
    }

    public static boolean isUserLoggedIn() {
        return Helper.authUser() != null;
    }

    public static User authUser() {
        Gson gson = new Gson();
        String data = Prefs.getString(AccountManager.AUTH_USER, "");
        if (data.isEmpty()) {
            return null;
        }
        return gson.fromJson(data, User.class);
    }

    public static void logout() {
        Prefs.remove(AccountManager.MOBILE);
        Prefs.remove(AccountManager.TOKEN);
        Prefs.remove(AccountManager.AUTH_USER);
    }


    public static void showAlert(Context context,String title,String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setIcon(R.drawable.ic_info_blue_24dp)
                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> dialog.dismiss()).show();
    }

    public static void showAlertLocation(Context context,String title,String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setIcon(R.drawable.ic_warning_black_24dp)
                .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    ((Activity)context).finish();
                }).show();
    }

}
