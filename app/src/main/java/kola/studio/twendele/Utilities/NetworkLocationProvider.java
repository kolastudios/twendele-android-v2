package kola.studio.twendele.Utilities;
/*
* NetworkLocationProvider  class converts geocode coordinates into human readable address.
*
* */

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;

public class NetworkLocationProvider {
    Context context;
    private LocationManager locationManager;
    private Location location;
    private String latitude,longitude;

    public NetworkLocationProvider(Context context) {
        this.context = context;
    }

    public String[] getLocation(){
        try {
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            return new String[]{latitude,longitude};
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isLocationEnabled(){
        int mode=0;
        LocationManager locationManager;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P){
            try{
                locationManager = (LocationManager) this.context.getSystemService(Context.LOCATION_SERVICE);
            }catch (Exception e){
                e.printStackTrace();
                Utils.log("Location status message: "+e.getMessage());
                return false;
            }
            assert locationManager != null;
            return locationManager.isLocationEnabled();
        }else{
            mode = Settings.Secure.getInt(this.context.getContentResolver(),Settings.Secure.LOCATION_MODE,Settings.Secure.LOCATION_MODE_OFF);
            return mode !=Settings.Secure.LOCATION_MODE_OFF;
        }
    }
}
