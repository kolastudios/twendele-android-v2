package kola.studio.twendele;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonObject;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.util.concurrent.TimeUnit;

import kola.studio.twendele.Models.AccountManager;
import kola.studio.twendele.Models.User;
import kola.studio.twendele.Utilities.Utils;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Zed on 3/22/2018.
 *
 */
public class APIClient {
    public static Retrofit getClient(Context mContext){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        Cache cache = new Cache(mContext.getCacheDir(),10 * 1024 * 1024);
        Utils.log("cache -> "+cache);
        Utils.log("cache max size -> "+cache.maxSize());
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60,TimeUnit.SECONDS)
                .cache(provideCache(mContext))
                .writeTimeout(30,TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    String token = Prefs.getString(AccountManager.TOKEN, "");
                    Utils.log("TOKEN->"+token);
                    Request request = chain.request()
                            .newBuilder()
                            .addHeader("Authorization", "Bearer " + token)
                            .addHeader("Accept", "application/json")
                            .build();
                    return chain.proceed(request);})
                .addNetworkInterceptor(provideCacheInterceptor(mContext))
                .addInterceptor(provideOfflineCacheInterceptor(mContext))
                .retryOnConnectionFailure(false)
                .build();

        GsonBuilder gsonBuilder = new GsonBuilder().setLenient();

        gsonBuilder.registerTypeAdapter(User.class, (JsonDeserializer<User>) (json, typeOfT, context) -> {
            JsonObject j = json.getAsJsonObject();
            Gson g = new Gson();
            User user = g.fromJson(json, User.class);
            user.setId(j.get("id").getAsLong());

            return user;
        });

        return new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .client(client)
                .build();
    }

    private static Cache provideCache(Context context){
        Cache cache = null;
        try{
            cache = new Cache(new File(context.getCacheDir(),"http-cache"),10*1024*1024);

        }catch(Exception e ){
            Utils.log("Couldn't create cache!");
        }
        return  cache;
    }

    /**
     * @param context
     * @return chain
     */
    private static Interceptor provideCacheInterceptor(Context context){
        return chain -> {
            Response response =chain.proceed(chain.request());
            CacheControl cacheControl;

            if(Utils.isNetworkAvailable(context)){
                cacheControl = new CacheControl.Builder().maxAge(0,TimeUnit.SECONDS).build();
            }else{
                cacheControl = new CacheControl.Builder().maxAge(7, TimeUnit.DAYS).build();
            }
            return response.newBuilder()
                    .removeHeader(Constants.HEADER_PRAGMA)
                    .removeHeader(Constants.HEADER_CACHE_CONTROL)
                    .header(Constants.HEADER_CACHE_CONTROL,cacheControl.toString())
                    .build();
        };
    }

    /**
     * When there is no internet connection
     * @param context
     * @return chain
     * */
    private static Interceptor provideOfflineCacheInterceptor(Context context){
        return chain -> {
            Request request = chain.request();
            if(!Utils.isNetworkAvailable(context)){
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(7,TimeUnit.DAYS)
                        .build();
                request = request.newBuilder()
                        .removeHeader(Constants.HEADER_PRAGMA)
                        .removeHeader(Constants.HEADER_CACHE_CONTROL)
                        .cacheControl(cacheControl)
                        .build();
            }
            return  chain.proceed(request);
        };
    }

}