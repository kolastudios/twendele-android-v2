package kola.studio.twendele;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}