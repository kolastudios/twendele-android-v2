package kola.studio.twendele;

import android.content.Context;
import android.content.ContextWrapper;

import com.orm.SugarApp;
import com.pixplicity.easyprefs.library.Prefs;

import es.dmoral.toasty.Toasty;
import kola.studio.twendele.Utilities.ConnectionDetector;

public class Twendele extends SugarApp {
    private ConnectionDetector connectionDetector;
    private static Twendele instance;
    public void onCreate() {
        super.onCreate();
        instance = this;
        new Prefs.Builder().setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        connectionDetector = new ConnectionDetector(instance);
        Toasty.Config.getInstance()
                .setTextSize(13).apply();
    }

    public  Context getContext(){
        return instance;
    }

    public static boolean isDeviceOnline() {
        return instance.connectionDetector.isNetworkAvailable();
    }

    public ConnectionDetector getConnectionDetector() {
        return instance.connectionDetector;
    }
}
